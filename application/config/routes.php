<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
// So much nicer than Drupal's
// Routes for migration
// Only relevant during development
$route['site/migrate'] = 'migrate';
$route['site/backup'] = 'migrate/backup';

// Routes for bobbleheads and collections
$route['bobblehead/create'] = 'bobblehead/create';
$route['bobblehead/edit/(:num)'] = 'bobblehead/edit/$1';
$route['collections/item/edit/(:num)'] = 'bobblehead/edit/$1';

$route['collections/create'] = 'collections/create';
$route['collections/edit/(:num)'] = 'collections/edit/$1';

$route['collections/(:num)'] = 'collections/index/$1';
$route['collections/(:any)/item/(:any)'] = 'bobblehead/item/$1/$2';
$route['collections/(:any)/(:num)'] = 'collections/collection/$1/$2';
$route['collections/(:any)'] = 'collections/collection/$1';
$route['collections'] = 'collections';

// Routes for searching
$route['search/json']['post'] = 'collections/search_json';
$route['search/(:any)/(:num)'] = 'collections/search/$1/$2';
$route['search/(:any)'] = 'collections/search/$1';
$route['search'] = 'collections/search';

// Routes for Custom Bobbleheads
$route['custom'] = 'custom_bobble/create';
$route['custom/create'] = 'custom_bobble/create';
$route['custom/list/(:num)'] = 'custom_bobble/review/$1';
$route['custom/list'] = 'custom_bobble/review';
$route['custom/details/(:num)'] = 'custom_bobble/details/$1';

// Routes for viewing news
$route['news/create'] = 'news/create';
$route['news/edit/(:num)']   = 'news/edit/$1';
$route['news/(:num)/(:num)/(:any)'] = 'news/view/$1/$2/$3';
$route['news'] = 'news';

// Routes for user interaction
$route['user/login'] = 'user/login';
$route['user/logout'] = 'user/logout';
$route['user/edit/(:num)'] = 'user/edit/$1';
$route['user/edit'] = 'user/edit';
$route['user'] = 'user';

// Routes for forum
$route['forum'] = 'forum';
$route['forum/manage'] = 'forum/manage';
$route['forum/edit/(:num)'] = 'forum/edit_forum/$1';
$route['forum/create'] = 'forum/create_forum';
$route['forum/f/(:any)'] = 'forum/view_forum/$1';
$route['forum/f/(:any)/new_topic'] = 'posts/make_post/$1';
$route['forum/t/(:num)/reply'] = 'posts/make_post/NULL/$1';
$route['forum/t/(:num)/(:any)'] = 'posts/view_topic/$1/$2';
$route['forum/t/(:num)'] = 'posts/view_topic/$1';
$route['forum/t'] = 'posts/view_topic';

// Routes for the store
$route['cart'] = 'cart/view';
$route['cart/add/(:num)']['post'] = 'cart/add/$1';
$route['cart/update/(:num)']['post'] = 'cart/update/$1';
$route['cart/checkout'] = 'cart/checkout';

// Our default Route goes to the Pages controller, which in turn loads a page
$route['default_controller'] = 'pages/view';
$route['(:any)'] = 'pages/view/$1';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
