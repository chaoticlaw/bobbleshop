<?php
defined('BASEPATH') OR exit('No direct script access allowed!');

class Post_model extends CI_Model {
  public function __construct() {
    parent::__construct();
  }

  public function get_posts_by_category($category_id = NULL, $get_topics_only = FALSE) {
    $replies = ($get_topics_only) ? '' : ' !='; 

    $query = $this->db->where('fp.forum_id', $category_id)
    ->where('reply_id'.$replies, NULL)
    ->order_by('date_created', 'DESC')
    ->join('user u', 'u.user_id = fp.user_id', 'INNER')
    ->from('forum_posts fp')
    ->select('fp.id, u.user_id, u.name, fp.forum_id, fp.reply_id, fp.title, fp.slug, fp.date_created, fp.date_modified')
    ->get();
    return $query->result_array();
  }

  public function get_topics_by_category($category_id = NULL) { 
    return $this->get_posts_by_category($category_id, TRUE);
  }

  public function get_posts_by_topic($slug = NULL, $topic_id = NULL) {
    // We can ensure the first result is always the first post by ordering the end result
    $query = $this->db->where('id =', $topic_id)
      ->or_where('reply_id =', $topic_id)
      ->order_by('date_created', 'ASC')
      ->join('user u', 'u.user_id = fp.user_id', 'INNER')
      ->from('forum_posts fp')
      ->select(
          'fp.id,
          u.user_id,
          u.name,
          fp.forum_id,
          fp.reply_id,
          fp.title,
          fp.slug,
          fp.date_created,
          fp.date_modified,
          fp.body')
      ->get();
    return $query->result_array();
  }

  public function get_posts_by_id ($topic_id = NULL) {
    $query = $this->db->where('id =', $topic_id)
      ->or_where('reply_id =', $topic_id)
      ->order_by('date_created', 'ASC')
      ->join('user u', 'u.user_id = fp.user_id', 'INNER')
      ->from('forum_posts fp')
      ->select(
          'fp.id,
          u.user_id,
          u.name,
          fp.forum_id,
          fp.reply_id,
          fp.title,
          fp.slug,
          fp.date_created,
          fp.date_modified,
          fp.body')
      ->get();
    return $query->result_array();
  }

  public function add_post($forum_id = NULL, $topic_id = NULL) {
    $this->load->helper('htmlpurifier');

    $body = html_purify($this->input->post('post_body', FALSE));
    $slug = url_title($this->input->post('title'), 'dash', TRUE);

    $data = array(
      'title'         => $this->input->post('title'),
      'slug'          => $slug,
      'forum_id'      => $forum_id,
      'reply_id'      => $topic_id,
      'user_id'       => $this->session->userdata('user_id'),
      'body'          => $body,
      'date_created'  => date('Y-m-d H:i:s')
    );

    $this->db->insert('forum_posts', $data);
    $insert_id = $this->db->insert_id();
    // If there is a reply_id that is not null, return the route to that topic instead.
    if (!empty($topic_id)) {
      return $topic_id.'/'.$slug;
    }
    return $insert_id.'/'.$slug;
  }

  public function count_replies_of_thread($thread_id = NULL) {
    $this->db->where('reply_id', $thread_id);
    $this->db->from('forum_posts');
    return $this->db->count_all_results();
  }

  public function get_recent_topics($post_amount = 5) {
    $query = $this->db->order_by('fp.date_created', 'DESC')
      ->limit($post_amount)
      ->select('fp.id, fp.reply_id, fp.title, fp.slug, fp.date_created')
      ->where('fp.reply_id', NULL)
      ->get('forum_posts fp');

    return $query->result_array();
  }
}
