<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Custom_bobble_model extends CI_Model {
  public function __construct() {
    parent::__construct();
  }

  public function create($image = NULL) {
    $this->load->helper('htmlpurifier');

    $description = html_purify($this->input->post('description', FALSE));

    $data = array(
      'user_id'             => $this->session->userdata('user_id'),
      'user_fullname'       => $this->input->post('user_fullname'),
      'user_email'          => $this->input->post('user_email'),
      'img_url'             => $image,
      'custom_description'  => $description,
      'bobblehead_id'       => $this->input->post('bobblehead_id')
    );

    return $this->db->insert('inquiries', $data);
  }

  public function get_inquiries($limit = 10, $offset_page = 0) {
    $this->db->limit($limit, $offset_page * $limit);
    $query = $this->db->get('inquiries');
    return $query->result_array();
  }

  public function get_inquiries_by_user_id($user_id = NULL, $limit =10, $offset_page = 0) {
    $this->db->limit($limit, $offset_page * $limit);
    $this->db->where(array('user_id' => $user_id));
    $query = $this->db->get('inquiries');
    return $query->result_array();
  }

  public function get_inquiry_by_id($id = NULL) {
    $this->db->limit(1);
    $this->db->where(array('inquiry_id' => $id));
    $query = $this->db->get('inquiries');
    return $query->row_array();
  }

  public function count_all_inquiries() {
    return $this->db->count_all('inquiries');
  }

  public function count_inquiries_by_user_id($id = NULL) {
    $this->db->where(array('user_id' => $id));
    return $this->db->count_all_results('inquiries');
  }


}
