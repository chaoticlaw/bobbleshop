<?php
defined('BASEPATH') OR exit('No direct script access allowed!');

class Forum_model extends CI_Model {
  public function __construct() {
    parent::__construct();
  }

  public function add_forum() {
    if (empty($this->input->post('slug'))) {
      $slug = url_title($this->input->post('name'), 'dash', TRUE);
    } else {
      $slug = $this->input->post('slug');
    }

    $data = array(
      'name'          => $this->input->post('name'),
      'slug'          => $slug,
      'description'   => $this->input->post('description'),
      'colour_code'   => $this->input->post('colour'),
      'date_created'  => date('Y-m-d H:i:s')
    );

    $this->db->insert('forum', $data);
    return $slug;
  }

  public function get_forum_by_slug($slug = NULL) {
    $this->db->limit(1);
    $query = $this->db->get_where('forum', array('slug' => $slug));
    return $query->row();
  }

  public function get_forum_by_topic($topic_id = NULL) {
    $query = $this->db->limit(1)
      ->join('forum_posts fp', 'fp.forum_id = f.id', 'INNER')
      ->select('
        f.id, f.name,
        f.description,
        f.slug, f.colour_code,
        f.date_created, f.date_modified
        ')
      ->from('forum f')
      ->get();

    return $query->row();
  }

  public function get_all_forums() {
    $query = $this->db->get('forum');
    return $query->result_array();
  }

  public function get_forum_by_id($id) {
    $query = $this->db->get_where('forum', ['id' => $id]);
      return $query->row();
  }

  public function update_forum($id) {
    if ($this->input->post('destroy')) {
      $this->db->where('id', $id);
      return $this->db->delete('forum');
    }

    if (empty($this->input->post('slug'))) {
      $slug = url_title($this->input->post('name'), 'dash', TRUE);
    } else {
      $slug = $this->input->post('slug');
    }

    $data = array(
      'name'          => $this->input->post('name'),
      'slug'          => $slug,
      'description'   => $this->input->post('description'),
      'colour_code'   => $this->input->post('colour'),
      'date_modified' => date('Y-m-d H:i:s')
    );

    $this->db->where('id', $id);
    return $this->db->update('forum', $data);
  }
}
