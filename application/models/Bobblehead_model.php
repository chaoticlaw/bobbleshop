<?php
defined('BASEPATH') OR exit('No direct script access allowed!');

class Bobblehead_model extends CI_Model {
  public function __construct() {
    parent::__construct();
  }

  public function get_bobbleheads($limit = 5, $offset_page = 0) {
    $this->db->limit($limit, $offset_page * $limit);
    $this->db->join('collections', 'collections.collection_id = bobblehead.collection_id');
    $query = $this->db->get('bobblehead');

    return $query->result_array();
  }

  public function get_all_bobbleheads() {
    $query = $this->db->get('bobblehead');
    return $query->result_array();
  }

  public function get_bobbleheads_by_search($search_term = NULL, $limit = 5, $offset_page = 0) {
    // This just searches the name of the bobblehead
    // Could potentially be expanded to the description
    // Use limit and offset
    $this->db->limit($limit, $offset_page * $limit);
    $this->db->like('name', $search_term);
    $this->db->join('collections', 'collections.collection_id = bobblehead.collection_id');
    $query = $this->db->get('bobblehead');
    return $query->result_array();
  }

  public function count_all_bobbleheads() {
    return $this->db->count_all('bobblehead');    
  }

  public function count_bobbleheads_search($search_term = NULL) {
    $this->db->like('name', $search_term);
    return $this->db->count_all_results('bobblehead');
  }

  public function get_bobblehead_by_id($id) {
    $this->db->limit(1);
    $query = $this->db->get_where('bobblehead', array('bobblehead_id' => $id));

    return $query->row_array();
  }

  public function get_bobblehead_by_slug($collection_slug, $bobble_slug) {
    $this->db->select('*');
    $this->db->from('bobblehead');
    $this->db->join('collections', 'collections.collection_id = bobblehead.collection_id');
    $this->db->where(array('bobblehead.slug' => $collection_slug, 'bobblehead.slug' => $bobble_slug));
    $this->db->limit(1);
    $query = $this->db->get();

    return $query->row_array();
  }

  public function get_bobbleheads_by_collection($collection_id, $limit = 5, $offset_page = 0) {
    $this->db->select('*');
    $this->db->from('bobblehead');
    $this->db->where(array('collection_id' => $collection_id));
    $this->db->limit($limit, $offset_page * $limit);
    $query = $this->db->get();
    return $query->result_array();
  }

  public function count_bobbleheads_by_collection($collection_id) {
    $this->db->where(array('collection_id' => $collection_id));
    return $this->db->count_all_results('bobblehead');
  }

  public function create_bobblehead($image = NULL) {
    $this->load->helper('url');
    $this->load->helper('htmlpurifier');

    $slug = url_title($this->input->post('name'), 'dash', TRUE);
    $description = html_purify($this->input->post('description', FALSE));

    $data = array(
      'name'          => $this->input->post('name'),
      'collection_id' => $this->input->post('collection_id'),
      'description'   => $description,
      'sale_date'     => $this->input->post('sale_date'),
      'img_url'       => $image,
      'slug'          => substr($slug, 0, 255),
      'price'         => $this->input->post('price'),
      'stock'         => $this->input->post('stock')
    );

    return $this->db->insert('bobblehead', $data);
  }

  public function update_bobblehead($id = NULL, $image = NULL) {
    if ($this->input->post('destroy')) {
      $this->db->where('bobblehead_id', $id);
      return $this->db->delete('bobblehead');
    }

    $this->load->helper('htmlpurifier');

    $slug = url_title($this->input->post('name'), 'dash', TRUE);
    $description = html_purify($this->input->post('description', FALSE));

    $data = array(
      'name'          => $this->input->post('name'),
      'collection_id' => $this->input->post('collection_id'),
      'description'   => $description,
      'sale_date'     => $this->input->post('sale_date'),
      'img_url'       => $image,
      'slug'          => substr($slug, 0, 255),
      'price'         => $this->input->post('price'),
      'stock'         => $this->input->post('stock')
    );

    $this->db->where('bobblehead_id', $id);
    return $this->db->update('bobblehead', $data);
  }

  public function reduce_inventory($product_id, $amount) {
    $product = $this->get_bobblehead_by_id($product_id);
    $new_quantity = $product['stock'] - $amount;

    $data = array(
      'stock' => $new_quantity
    );
    $query = $this->db->where('bobblehead_id', $product_id);
    return $query->update('bobblehead', $data);
  }
}
