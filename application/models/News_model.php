<?php
defined('BASEPATH') OR exit('No direct script access allowed!');

class News_model extends CI_Model {
  public function __construct() {
    // Load database functionality
    //$this->load->database();
    // No longer necessary since we autoload it under /application/config/autoload.php
    parent::__construct();
  }

  public function get_news($limit = FALSE, $year = FALSE, $month = FALSE, $slug = FALSE ) {
    // get_news(limit, year, month, slug)
    // Get either a list of articles, or a single one

    if ($slug === FALSE || $year === FALSE || $month === FALSE) {
      // If there is no slug, return all results
      // Return it in descending order
      $this->db->order_by('news_date', 'desc');

      // If we need to limit the result, do so.
      if ($limit > 0) {
        $this->db->limit($limit);
      }
      // We use our view to make this good.
      $query = $this->db->get('view_nice_news');
      return $query->result_array();
    }

    $this->db->limit(1);
    $query = $this->db->get_where('view_nice_news', array('slug' => $slug, 'post_year' => $year, 'post_month' => $month));
    return $query->row_array();
  }

  public function get_five_latest_articles() {
    $query = $this->news_model->get_news(5);
    return $query;
  }

  public function get_news_by_id($id) {
    $this->db->limit(1);
    $query = $this->db->get_where('news', array('news_id' => $id));
    return $query->row_array();
  }

  public function add_news() {
    // Load the url helper so we can make the slug
    $this->load->helper('url');
    // External library htmlpurifier will process the content
    $this->load->helper('htmlpurifier');

    $slug = url_title($this->input->post('title'), 'dash', TRUE);
    $body = html_purify($this->input->post('content', FALSE));

    $data = array(
      'news_title'   => $this->input->post('title'),
      'news_date'    => $this->input->post('date'),
      'news_content' => $body,
      'slug'         => substr($slug, 0, 114)
    );

    return $this->db->insert('news', $data);
  }

  public function edit_news($id = NULL) {
    if ($this->input->post('delete') !== NULL) {
      $this->db->where('news_id', $id);
      return $this->db->delete('news');
    }

    $this->load->helper('url');
    $this->load->helper('htmlpurifier');

    $slug = url_title($this->input->post('title'), 'dash', TRUE);
    $body = html_purify($this->input->post('content', FALSE));

    $data = array(
      'news_title'   => $this->input->post('title'),
      'news_date'    => $this->input->post('date'),
      'news_content' => $body,
      'slug'         => substr($slug, 0, 114)
    );
    $this->db->where('news_id', $id);
    return $this->db->update('news', $data);
  }

}