<?php
defined('BASEPATH') OR exit('No direct script access allowed!');

class Cart_model extends CI_Model {
  public function __construct() {
    $this->load->model('bobblehead_model');
    parent::__construct();
  }

  public function get_by_session() {
    $session = $this->session->userdata('cart_session');
    $query = $this->db->select('*')
    ->where('session_id', $session)
    ->from('cart c')
    ->join('bobblehead b', 'c.product_id = b.bobblehead_id', 'left')
    ->get();
    return $query->result_array();
  }

  public function get_cart_by_id($cart_id = NULL) {
    $query = $this->db->get_where('cart', array('cart_id' => $cart_id));
    return $query->row_array();
  }

  public function add_to_cart($product_id) {
    $data = array(
      'product_id'  => $product_id,
      'product_quantity'    => $this->input->post('quantity'),
      'session_id'  => $this->session->userdata('cart_session')
    );

    return $this->db->insert('cart', $data);
  }

  public function update_cart($cart_id) {
    if ($this->input->post('destroy')) {
      $this->db->where('cart_id', $cart_id);
      return $this->db->delete('cart');
    } else {
      $data = array(
        'product_quantity' => $this->input->post('quantity')
      );

      $this->db->where('cart_id', $cart_id);
      return $this->db->update('cart', $data);
    }
  }

  public function create_purchase() {
    // Grab email from form
    $email = $this->input->post('email');
    // If user_id is available, use it
    $user_id = ($this->session->userdata('user_id') ? $this->session->userdata('user_id') : NULL);
    $data = array(
      'email' => $email,
      'user_id' => $user_id,
      'name'  => $this->input->post('name'),
      'address'  => $this->input->post('address'),
      'suburb'  => $this->input->post('suburb'),
      'postcode'  => $this->input->post('postcode'),
      'aus_state'  => $this->input->post('aus_state')
    );
    $this->db->insert('purchases', $data);
    return $this->db->insert_id();
  }

  public function bag_purchases($purchase_id) {
    $cart = $this->get_by_session();
    // Grab each item in the cart, and insert it
    $total = 0.00;
    foreach ($cart as $item) {
      $price = $this->bobblehead_model->get_bobblehead_by_id($item['product_id'])['price'];

      $data = array(
        'purchase_id' => $purchase_id,
        'item_id'     => $item['product_id'],
        'quantity'    => $item['product_quantity'],
        'unit_price'  => $price
      );
      $total = $total + ($item['product_quantity'] * $price);
      $this->db->insert('purchase_items', $data);
      $this->bobblehead_model->reduce_inventory($item['product_id'], $item['product_quantity']);
    }

    // Calculated total should then be written to purchases
    $total = array(
      'cost_total'  => $total
    );
    return $this->db->where('purchase_id', $purchase_id)
    ->update('purchases', $total);
  }

  public function clear_cart() {
    $cart_session = $this->session->userdata('cart_session');
    return $this->db->where('session_id', $cart_session)
    ->delete('cart');
  }
}
