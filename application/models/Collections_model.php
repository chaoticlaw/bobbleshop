<?php
defined('BASEPATH') OR exit('No direct script access allowed!');

class Collections_model extends CI_Model {
  public function __construct() {
    parent::__construct();
  }

  public function get_all_collections() {
    $this->db->order_by('collection_name', 'asc');
      $query = $this->db->get('collections');
      return $query->result_array();
  }

  public function get_collection_by_slug($slug) {
    $this->db->limit(1);
    $query = $this->db->get_where('collections', array('collection_slug' => $slug));
    return $query->row_array();
  }

  public function get_collection_by_id($id) {
    $this->db->limit(1);
    $query = $this->db->get_where('collections', array('collection_id' => $id));
    return $query->row_array();
  }

  public function create_collection() {
    $this->load->helper('htmlpurifier');

    $slug = url_title($this->input->post('name'), 'dash', TRUE);
    $description = html_purify($this->input->post('description', FALSE));

    $data = array(
      'collection_name'         => $this->input->post('name'),
      'collection_slug'         => substr($slug, 0, 255),
      'collection_description'  => $description
    );

    return $this->db->insert('collections', $data);
  }


  public function update_collection($id) {
    $this->load->helper('htmlpurifier');

    $slug = url_title($this->input->post('name'), 'dash', TRUE);
    $description = html_purify($this->input->post('description', FALSE));

    $data = array(
      'collection_name'         => $this->input->post('name'),
      'collection_slug'         => substr($slug, 0, 255),
      'collection_description'  => $description
    );

    $this->db->where('collection_id', $id);
    return $this->db->update('collections', $data);
  }
}
