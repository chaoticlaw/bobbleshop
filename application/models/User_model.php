<?php
defined('BASEPATH') OR exit('No direct script access allowed!');

class User_model extends CI_Model {
  public function __construct() {
    parent::__construct();
  }

  public function register_user() {
    $data = array(
      'name'          => $this->input->post('name'),
      'email'         => $this->input->post('email'),
      'auth_hashsalt' => password_hash($this->input->post('password'), PASSWORD_DEFAULT)
    );

    // Super basic check to give users their privileges
    // Essentially the first user to register should get admin privileges
    $user_rows = $this->db->count_all('user');
    if ( $user_rows < 1) {
      $data['user_level'] = 'admin';
    } else {
      $data['user_level'] = 'user';
    }

    return $this->db->insert('user', $data);
  }

  public function get_user_by_email($email) {
    $this->db->limit(1);
    $query = $this->db->get_where('user', array('email' => $email));

    return $query->row();
  }

  public function get_user_by_id($id) {
    $this->db->limit(1);
    $query = $this->db->get_where('user', array('user_id' => $id));

    return $query->row();
  }

  public function get_all_users($limit = 10, $offset_page = 0) {
    $this->db->limit($limit, $offset_page * $limit);
    $query = $this->db->get('user');

    return $query->result_array();
  }

  public function count_all_users() {
    return $this->db->count_all('user');
  }

  public function update_user($id) {
    if ($this->input->post('destroy')) {
      $this->db->where('user_id', $id);
      return $this->db->delete('user');
    }

    $data = array(
      'name'        => $this->input->post('name'),
      'email'       => $this->input->post('email'),
    );

    // Don't change the password unless they set a new one
    if (!empty($this->input->post('password'))) {
      $data['auth_hashsalt'] = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
    }

    // An admin might choose to edit other user details, so we can't just pull user_id from session
    $this->db->where('user_id', $id);
    return $this->db->update('user', $data);
  }
  
}
