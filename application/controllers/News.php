<?php
defined('BASEPATH') OR exit('No direct script access allowed!');

class News extends CI_Controller {
  public function __construct() {
    parent::__construct();
    $this->load->model('news_model');
    $this->load->helper('url_helper');
    if(empty($this->session->userdata('cart_session'))) {
      $this->session->set_userdata('cart_session', uniqid(time().session_id(), true));
    }
    
    if(ENVIRONMENT !== 'production') $this->output->enable_profiler(TRUE);
  }

  // Get all news
  public function index() {
    $data['news'] = $this->news_model->get_news();
    $data['title'] = 'News Archive';

    $this->load->view('templates/header', $data);
    $this->load->view('news/index', $data);
    $this->load->view('templates/footer', $data);
  }

  // Get single article
  public function view($year = NULL, $month = NULL, $slug = NULL) {
    $data['news_item'] = $this->news_model->get_news(FALSE, $year, $month, $slug);

    if (empty($data['news_item'])) {
      show_404();
    }

    $data['title'] = $data['news_item']['news_title'];

    $this->load->view('templates/header', $data);
    $this->load->view('news/view', $data);
    $this->load->view('templates/footer', $data);
  }

  public function create() {
    // Don't allow a user to create a new article if they don't have the credentials
    // Essentially, check if they are logged in and have admin status
    if(!$this->session->userdata('logged_in') && $this->session->userdata('user_level') !== 'admin') {
      $this->session->set_flashdata('message_type,', 'error');
      $this->session->set_flashdata('message', 'You don’t have permission to do that.');
      redirect('');
    } else {
      // Pull useful functions from CodeIgniter
      $this->load->helper('form');
      $this->load->library('form_validation');

      $data['title'] = 'Create new news article';

      // We set the 'required' rule on the title form element
      $this->form_validation->set_rules('title', 'News Title', 'required');
      $this->form_validation->set_rules('content', 'News Content', 'required');

      if ($this->form_validation->run() === FALSE) {
        $this->load->view('templates/header', $data);
        $this->load->view('news/news_form', $data);
        $this->load->view('templates/footer', $data);
      } else {
        $this->news_model->add_news();
        $this->load->view('templates/header', $data);
        $this->load->view('news/success', $data);
        $this->load->view('templates/footer', $data);
      }
    }

  }

  public function edit($article_id = NULL) {
    // User session check
    if(!$this->session->userdata('logged_in') && $this->session->userdata('user_level') !== 'admin') {
      $this->session->set_flashdata('message_type,', 'error');
      $this->session->set_flashdata('message', 'You don’t have permission to do that.');
      redirect('');
    }
    $this->load->helper('form');
    $this->load->library('form_validation');

    $this->form_validation->set_rules('title', 'News Title', 'required');
    $this->form_validation->set_rules('content', 'News Content', 'required');

    $data['title'] = 'Edit existing news article';

    $data['news_item'] = $this->news_model->get_news_by_id($article_id);

    if (empty($data['news_item']) && $this->input->post() !== NULL) {
      if ($article_id !== NULL) {
            $this->session->set_flashdata('message','The news item of ID #'.$article_id.' could not be found.');
      } else {
            $this->session->set_flashdata('message','No news article was specified for editing.');
      }
      redirect('news/create');
    }

    if ($this->form_validation->run() === FALSE) {
      $this->load->view('templates/header', $data);
      $this->load->view('news/news_form', $data);
      $this->load->view('templates/footer', $data);
    } else {
      $this->news_model->edit_news($article_id);
      $this->load->view('templates/header', $data);
      $this->load->view('news/success', $data);
      $this->load->view('templates/footer', $data);
    }
  }
}

