<?php
defined('BASEPATH') OR exit('No direct script access allowed!');

class Cart extends CI_Controller {
  public function __construct() {
    parent::__construct();
    $this->load->model('cart_model');
    $this->load->model('bobblehead_model');
    $this->load->helper('flash_bobblehead_notification_helper');
    // Set userdata with the session value for a unique value because CodeIgniter cycles through it
    if(empty($this->session->userdata('cart_session'))) {
      $this->session->set_userdata('cart_session', uniqid(time().session_id(), true));
    }

    if(ENVIRONMENT !== 'production') $this->output->enable_profiler(TRUE);
  }

  // Get all items by session_id
  public function view() {
    $this->load->helper('form');
    $data['cart'] = $this->cart_model->get_by_session();
    $data['title'] = 'Cart';

    $this->load->view('templates/header', $data);
    $this->load->view('cart/view', $data);
    $this->load->view('templates/footer', $data);
  }

  // Add an item to the cart
  public function add($product_id = NULL, $quantity = 1) {
    $this->load->helper('form');
    $this->load->library('form_validation');
    $this->form_validation->set_rules('quantity', 'required|integer|callback_enough_items[$amount]');

    // Assuming a person chooses to add a number to the cart
      if ($this->input->post('quantity')) {
        $quantity = $this->input->post('quantity');
      }

    // If the product_id is valid
    if(empty($this->bobblehead_model->get_bobblehead_by_id($product_id))) {
      flash_bobble_notification('error', 'You cannot add that item to your cart');
      redirect('cart');
    } else {
      // Then add the product to the cart
      $this->cart_model->add_to_cart($product_id);
      flash_bobble_notification('success', 'Successfully added to cart!');
      redirect('cart');
    }

  }

  // Update the cart
  public function update($cart_id = NULL) {

    // When you have a valid cart item
    $cart = $this->cart_model->get_cart_by_id($cart_id);

    if ($cart['session_id'] !== $this->session->userdata('cart_session')) {
      flash_bobble_notification('error', 'We were unable to update that cart');
      redirect('cart');
    }
    // If the cart_session matches the user data

    // And that the stock doesn't drop below zero
    if (empty($amount = $this->input->post('quantity')) && !is_numeric($amount)) {
      flash_bobble_notification('error', 'A quanitity must be specified when updating');
      redirect('cart');
    }
    $product = $this->bobblehead_model->get_bobblehead_by_id($cart['product_id']);
    if ($amount > $product['stock']) {
      flash_bobble_notification('error', 'You have exceeded the amount of stock available!');
      redirect('cart');
    }
    // Then update the cart item
    $this->cart_model->update_cart($cart_id);
    // Notify user
    flash_bobble_notification('success', 'Item updated successfully');
    // Redirect back to cart
    redirect('cart');
  }

  public function checkout() {
    $this->load->helper('form');
    $this->load->library('form_validation');
    $this->load->model('user_model');
    $data['user'] = $this->user_model->get_user_by_id($this->session->userdata('user_id'));
    $data['cart'] = $this->cart_model->get_by_session();
    // If the cart is empty, we should not let them checkout nothing
    if (empty($data['cart'])) {
      flash_bobble_notification('error', 'You cannot checkout with an empty cart.');
      redirect('cart');
    }

    $data['title'] = 'Checkout';
    $this->form_validation->set_rules('name','Name','required');
    $this->form_validation->set_rules('email','E-mail','required|valid_email');
    $this->form_validation->set_rules('address','Address','required');
    $this->form_validation->set_rules('suburb','Suburb','required');
    $this->form_validation->set_rules('postcode','Postcode','required|exact_length[4]');
    $this->form_validation->set_rules('aus_state','State','required|in_list[NSW,ACT,TAS,VIC,SA,WA,NT,QLD]');


    if ($this->form_validation->run() === FALSE) {
      // Show the form
      $this->load->view('templates/header', $data);
      $this->load->view('cart/checkout', $data);
      $this->load->view('templates/footer', $data);
    } else {
      $this->db->trans_start();
      // Upon submit, we should create a purchase entry
      $purchase_id = $this->cart_model->create_purchase();

      // Then copy the values from cart table to purchase_items
      $this->cart_model->bag_purchases($purchase_id);
      // And then clear the cart
      $this->cart_model->clear_cart();
      // And clear the session so we get a new one
      $this->session->unset_userdata('cart_session');
      $this->db->trans_complete();

      // Finally redirect the user
      flash_bobble_notification('success', 'Items were successfully ordered!');
      redirect('');
    }
  }
}
