<?php
defined('BASEPATH') OR exit('No direct script access allowed!');

class Collections extends CI_Controller {
  public function __construct() {
    parent::__construct();
    $this->load->model('collections_model');
    $this->load->model('bobblehead_model');
    if(empty($this->session->userdata('cart_session'))) {
      $this->session->set_userdata('cart_session', uniqid(time().session_id(), true));
    }
    
    if(ENVIRONMENT !== 'production') $this->output->enable_profiler(TRUE);
  }

  // See all collections
  public function index($offset_page = 1) {
    $data['title'] = 'All Bobbleheads';
    $data['collections'] = $this->collections_model->get_all_collections();
    $data['bobblehead_array'] = $this->bobblehead_model->get_bobbleheads(5, $offset_page - 1);
    $data['result_count'] = $this->bobblehead_model->count_all_bobbleheads();
    $data['description'] = 'All bobbleheads are shown here. It\'s good.';

    $pagination_conf['base_url'] = base_url('collections/');
    $pagination_conf['total_rows'] = $data['result_count'];
    $pagination_conf['per_page'] = 5;
    $pagination_conf['use_page_numbers'] = true;
    $pagination_conf['full_tag_open'] = '<ul class="pagination">';
    $pagination_conf['full_tag_close'] = '</ul>';
    $pagination_conf['first_tag_open'] = '<li>';
    $pagination_conf['first_tag_close'] = '</li>';
    $pagination_conf['last_tag_open'] = '<li>';
    $pagination_conf['last_tag_close'] = '</li>';
    $pagination_conf['next_tag_open'] = '<li>';
    $pagination_conf['next_tag_close'] = '</li>';
    $pagination_conf['prev_tag_open'] = '<li>';
    $pagination_conf['prev_tag_close'] = '</li>';
    $pagination_conf['cur_tag_open'] = '<li class="active"><a href="#">';
    $pagination_conf['cur_tag_close'] = '</a></li>';
    $pagination_conf['num_tag_open'] = '<li>';
    $pagination_conf['num_tag_close'] = '</li>';

    $this->load->library('pagination');
    $this->pagination->initialize($pagination_conf);

    $this->load->view('templates/header.php', $data);
    $this->load->view('collections/index.php', $data);
    $this->load->view('templates/footer.php', $data);
  }

  // View a specific collection
  public function collection($slug, $offset_page = 1) {
    $data['all_collections'] = $this->collections_model->get_all_collections();
    $data['collection'] = $this->collections_model->get_collection_by_slug($slug);

    if(empty($data['collection'])) {
      $this->session->set_flashdata('message_type', 'error');
      $this->session->set_flashdata('message', 'We could not find the specified collection');
      redirect('collections');
    }

    $data['bobblehead_array'] = $this->bobblehead_model->get_bobbleheads_by_collection($data['collection']['collection_id'], 5, $offset_page - 1);

    $data['bobblehead_count'] = $this->bobblehead_model->count_bobbleheads_by_collection($data['collection']['collection_id']);

    $pagination_conf['base_url'] = base_url('collections/'.$slug.'/');
    $pagination_conf['total_rows'] = $data['bobblehead_count'];
    $pagination_conf['per_page'] = 5;
    $pagination_conf['use_page_numbers'] = true;
    $pagination_conf['full_tag_open'] = '<ul class="pagination">';
    $pagination_conf['full_tag_close'] = '</ul>';
    $pagination_conf['first_tag_open'] = '<li>';
    $pagination_conf['first_tag_close'] = '</li>';
    $pagination_conf['last_tag_open'] = '<li>';
    $pagination_conf['last_tag_close'] = '</li>';
    $pagination_conf['next_tag_open'] = '<li>';
    $pagination_conf['next_tag_close'] = '</li>';
    $pagination_conf['prev_tag_open'] = '<li>';
    $pagination_conf['prev_tag_close'] = '</li>';
    $pagination_conf['cur_tag_open'] = '<li class="active"><a href="#">';
    $pagination_conf['cur_tag_close'] = '</a></li>';
    $pagination_conf['num_tag_open'] = '<li>';
    $pagination_conf['num_tag_close'] = '</li>';


    $this->load->library('pagination');
    $this->pagination->initialize($pagination_conf);

    $data['title'] = $data['collection']['collection_name'];

    $this->load->view('templates/header.php', $data);
    $this->load->view('collections/view_collection.php', $data);
    $this->load->view('templates/footer.php', $data);
  }

  public function search_json($search_term = null) {
    $this->output->enable_profiler(FALSE);
    $search_term = $this->input->post('search_term');
    $data['result'] = $this->bobblehead_model->get_bobbleheads_by_search($search_term, 5);
    // Return as JSON
    return $this->output
      ->set_content_type('application/json')
      ->set_output(json_encode($data['result']));
  }

  public function search($search_term = NULL, $offset_page = 1) {
    if (!empty($this->input->post('search_term'))) {
      redirect(base_url('search/'.$this->input->post('search_term')));
    }
    $data['collections'] = $this->collections_model->get_all_collections();
    $data['description'] = 'Searching';

    if (empty($search_term)) {
      $this->session->set_flashdata('message_type', 'error');
      $this->session->set_flashdata('message', 'You must enter in at least one search term');
      redirect('collections');
    } else {
      $data['result_count'] = $this->bobblehead_model->count_bobbleheads_search($search_term);
      $data['bobblehead_array'] = $this->bobblehead_model->get_bobbleheads_by_search($search_term, 5, $offset_page - 1);
    }

    $pagination_conf['base_url'] = base_url('search/'.$search_term.'/');
    $pagination_conf['total_rows'] = $data['result_count'];
    $pagination_conf['per_page'] = 5;
    $pagination_conf['use_page_numbers'] = true;
    $pagination_conf['full_tag_open'] = '<ul class="pagination">';
    $pagination_conf['full_tag_close'] = '</ul>';
    $pagination_conf['first_tag_open'] = '<li>';
    $pagination_conf['first_tag_close'] = '</li>';
    $pagination_conf['last_tag_open'] = '<li>';
    $pagination_conf['last_tag_close'] = '</li>';
    $pagination_conf['next_tag_open'] = '<li>';
    $pagination_conf['next_tag_close'] = '</li>';
    $pagination_conf['prev_tag_open'] = '<li>';
    $pagination_conf['prev_tag_close'] = '</li>';
    $pagination_conf['cur_tag_open'] = '<li class="active"><a href="#">';
    $pagination_conf['cur_tag_close'] = '</a></li>';
    $pagination_conf['num_tag_open'] = '<li>';
    $pagination_conf['num_tag_close'] = '</li>';


    $this->load->library('pagination');
    $this->pagination->initialize($pagination_conf);
    $data['title'] = 'Searching for \''.$search_term.'\'';

    $this->load->view('templates/header.php', $data);
    $this->load->view('collections/index.php', $data);
    $this->load->view('templates/footer.php', $data);
    //$this->output->enable_profiler(TRUE);
  }

  // Create a collection
  public function create() {
    if(!$this->session->userdata('logged_in') && $this->session->userdata('user_level') !== 'admin') {
      $this->session->set_flashdata('message_type,', 'error');
      $this->session->set_flashdata('message', 'You don’t have permission to do that.');
      redirect('');
    }
    $this->load->helper('form');
    $this->load->library('form_validation');

    $this->form_validation->set_rules('name', 'Collection Name', 'required');
    $this->form_validation->set_rules('description', 'Description', 'required');

    $data['title'] = 'Create collection';

    if($this->form_validation->run() === FALSE) {
      $this->load->view('templates/header', $data);
      $this->load->view('collections/add_edit_form', $data);
      $this->load->view('templates/footer', $data);
    } else {
      $this->collections_model->create_collection();
      $this->session->set_flashdata('message_type', 'success');
      $this->session->set_flashdata('message', 'Collection created!');
      redirect('collections');
    }
  }

  // Edit a collection
  public function edit($id = NULL) {
    if(!$this->session->userdata('logged_in') && $this->session->userdata('user_level') !== 'admin') {
      $this->session->set_flashdata('message_type,', 'error');
      $this->session->set_flashdata('message', 'You don’t have permission to do that.');
      redirect('');
    }
    $this->load->helper('form');
    $this->load->library('form_validation');

    $data['collection'] = $this->collections_model->get_collection_by_id($id);

    $data['title'] = 'Editing collection';
    if (empty($id) || empty($data['collection'])) {
      $this->session->set_flashdata('message_type', 'error');
      $this->session->set_flashdata('message', 'ID was not specified, or does not exist');
      redirect('collections');
    }

    if($this->form_validation->run() === FALSE) {
      $this->load->view('templates/header', $data);
      $this->load->view('collections/add_edit_form', $data);
      $this->load->view('templates/footer', $data);
    } else {
      $this->collections_model->update_collection($id);
      $this->session->set_flashdata('message_type', 'success');
      $this->session->set_flashdata('message', 'Collection edited!');
      redirect('collections/'.$data['collection']['collection_slug']);
    }
  }
}
