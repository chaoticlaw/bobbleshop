<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
  public function __construct() {
    parent::__construct();
    $this->load->model('user_model');
    $this->load->helper('url_helper');
    if(empty($this->session->userdata('cart_session'))) {
      $this->session->set_userdata('cart_session', uniqid(time().session_id(), true));
    }
    
    if(ENVIRONMENT !== 'production') $this->output->enable_profiler(TRUE);
  }

  public function index() {
    if(!$this->session->userdata('logged_in')) {
      $this->session->set_flashdata('message_type', 'error');
      $this->session->set_flashdata('message', 'You must login to do that.');
      redirect('user/login');
    } else {
      // Get details of current user so we can display them
      $data['user'] = $this->user_model->get_user_by_id($this->session->userdata('user_id'));
      $data['title'] = 'User';
      $this->load->view('templates/header', $data);
      $this->load->view('user/index', $data);
      $this->load->view('templates/footer', $data);
    }
  }

  public function browse($offset_page = 1) {
    if(!$this->session->userdata('logged_in')) {
      $this->session->set_flashdata('message_type', 'error');
      $this->session->set_flashdata('message', 'You are not logged in to do that.');
      redirect('user/login');
    } elseif (!$this->session->userdata('user_level') === 'admin') {
      $this->session->set_flashdata('message_type', 'error');
      $this->session->set_flashdata('message', 'You do not have permission to do that.');
      redirect('user');
    } else {
      $data['title'] = 'Admin - User Index';

      $data['users'] = $this->user_model->get_all_users(10, $offset_page - 1);
      $data['user_count'] = $this->user_model->count_all_users();

      $pagination_conf['base_url'] = base_url('user/browse');
      $pagination_conf['total_rows'] = $data['user_count'];
      $pagination_conf['per_page'] = 10;
      $pagination_conf['use_page_numbers'] = true;
      $pagination_conf['full_tag_open'] = '<ul class="pagination">';
      $pagination_conf['full_tag_close'] = '</ul>';
      $pagination_conf['first_tag_open'] = '<li>';
      $pagination_conf['first_tag_close'] = '</li>';
      $pagination_conf['last_tag_open'] = '<li>';
      $pagination_conf['last_tag_close'] = '</li>';
      $pagination_conf['next_tag_open'] = '<li>';
      $pagination_conf['next_tag_close'] = '</li>';
      $pagination_conf['prev_tag_open'] = '<li>';
      $pagination_conf['prev_tag_close'] = '</li>';
      $pagination_conf['cur_tag_open'] = '<li class="active"><a href="#">';
      $pagination_conf['cur_tag_close'] = '</a></li>';
      $pagination_conf['num_tag_open'] = '<li>';
      $pagination_conf['num_tag_close'] = '</li>';

      $this->load->library('pagination');
      $this->pagination->initialize($pagination_conf);

      $this->load->view('templates/header', $data);
      $this->load->view('user/admin_user_list', $data);
      $this->load->view('templates/footer', $data);
    }


  }

  public function edit($user_id = NULL) {
    if(!$this->session->userdata('logged_in')) {
      $this->session->set_flashdata('message_type', 'error');
      $this->session->set_flashdata('message', 'You must login to do that.');
      redirect('user/login');
    } elseif ($user_id !== NULL && $this->session->userdata('user_id') !== $user_id && $this->session->userdata('user_level') !== 'admin') {
      // Error for if a user is not authorised
      $this->session->set_flashdata('message_type', 'error');
      $this->session->set_flashdata('message', 'You are not authorised to do that.');
      redirect('user');
    } elseif ($user_id !== NULL && empty($this->user_model->get_user_by_id($user_id))) {
      // Error if the user to edit doesn't exist
      $this->session->set_flashdata('message_type', 'error');
      $this->session->set_flashdata('message', 'User with id #'.$user_id.' does not exist.');
      redirect('user');
    } else {
      // Display form to edit user data
      $this->load->helper('form');
      $this->load->library('form_validation');

      $this->form_validation->set_rules('name', 'Name', 'required');
      $this->form_validation->set_rules('email', 'E-mail', 'required|valid_email|callback_email_unique[$user_id]');

      $data['title'] = 'Edit profile';

      if ($user_id === NULL) {
        $data['user'] = $this->user_model->get_user_by_id($this->session->userdata('user_id'));
      } else {
        $data['user'] = $this->user_model->get_user_by_id($user_id);
      }

      if ($this->form_validation->run() === FALSE) {
        
        $this->load->view('templates/header', $data);
        $this->load->view('user/edit', $data);
        $this->load->view('templates/footer', $data);
      } else {

        // Update the user
        //var_dump($this->input->post());
        $this->user_model->update_user($data['user']->user_id);

        $data['session_update'] = $this->user_model->get_user_by_id($data['user']->user_id);
        // If there's nothing left in the session, this means the user was deleted
        // Also check if the logged in user was the one deleted and clear the session
        if (empty($data['session_update'])) {
          if ($user_id === $this->session->userdata('user_id')) {
            $session_items = array('user_id', 'name', 'email', 'user_level', 'logged_in');
            $this->session->unset_userdata($session_items);
            $this->session->set_flashdata('message_type', 'success');
            $this->session->set_flashdata('message', 'Your user account has been deleted.');
            redirect('');
          } else {
            $this->session->set_flashdata('message_type', 'success');
            $this->session->set_flashdata('message', 'User deleted successfully.');
            redirect('user/browse');
          }
          
        }

        // Update session data if we're updating ourselves
        if ($data['session_update']['user_id'] === $this->session->userdata('user_id')) {
          $session_items = array(
            'user_id'     => $data['session_update']->user_id,
            'email'       => $data['session_update']->email,
            'name'        => $data['session_update']->name,
            'user_level'  => $data['session_update']->user_level
          );

          $this->session->set_userdata($session_items);
        }

        $this->session->set_flashdata('message_type', 'success');
        $this->session->set_flashdata('message', 'User details have been updated successfully!');
        
        //var_dump($this->input->post());

        redirect('user/edit/'.$data['user']->user_id);
      }
    }
  }

  public function email_unique($string, $user_id) {
    $current_user = $this->user_model->get_user_by_id($user_id);
    $check = !empty($this->user_model->get_user_by_email($string));

    // If the submitted email is different to the one in the db, and does not return another user, return true
    if ($string !== $current_user['email'] || !$check) {
      return TRUE;
    } else {
      $this->form_validation->set_message('email_unique', 'Your e-mail is not unique');
      return FALSE;
    }
  }

  public function register() {
    if(!empty($_SESSION['logged_in'])) {
      $this->session->set_flashdata('message_type', 'error');
      $this->session->set_flashdata('message', 'You’re already logged in!');
      redirect('user');
    } else {
      $this->load->helper('form');
      $this->load->library('form_validation');

      $this->form_validation->set_rules('name', 'Name', 'required');
      $this->form_validation->set_rules('email', 'E-mail', 'required|valid_email|is_unique[user.email]');
      $this->form_validation->set_rules('password', 'Password', 'required');
      $this->form_validation->set_rules('password_confirm', 'Password confirmation', 'matches[password]');

      $data['title'] = 'Register user';

      if ($this->form_validation->run() === FALSE) {
        $this->load->view('templates/header', $data);
        $this->load->view('user/user_register_form', $data);
        $this->load->view('templates/footer', $data);
      } else {
        $this->user_model->register_user();
        $this->session->set_flashdata('message_type', 'success');
        $this->session->set_flashdata('message', 'You’ve registered successfully! Use your credentials to log in.');
        // Redirect user to login
        redirect('user/login');
      }

    }
  }

  public function login() {
    if(!empty($_SESSION['logged_in'])) {

      $this->session->set_flashdata('message_type', 'error');
      $this->session->set_flashdata('message', 'You’re already logged in!');
      redirect('user');

    } else {

      $this->load->helper('form');
      $this->load->library('form_validation');

      $this->form_validation->set_rules('email', 'E-mail', 'required|valid_email');
      $this->form_validation->set_rules('password', 'Password', 'required');

      $data['title'] = 'Login';

      if ($this->form_validation->run() === FALSE) {
        $this->load->view('templates/header', $data);
        $this->load->view('user/user_login_form', $data);
        $this->load->view('templates/footer', $data);

      } else {

        $user = $this->user_model->get_user_by_email($this->input->post('email'));
        

        if ( !empty($user) && $verification_passed = password_verify($this->input->post('password'), $user->auth_hashsalt)) {
          // Set session details
          $session_items = array(
            'user_id'     => $user->user_id,
            'email'       => $user->email,
            'name'        => $user->name,
            'user_level'  => $user->user_level,
            'logged_in'   => TRUE
          );

          $this->session->set_userdata($session_items);
          $this->session->set_flashdata('message_type','success');
          $this->session->set_flashdata('message', 'Logged in successfully!');
          redirect('user');
        } else {
          $this->session->set_flashdata('message_type', 'error');
          $this->session->set_flashdata('message', 'Your credentials could not be validated. Please check your details and try again.');
          redirect('user/login');
        }
      }
    }
  }

  public function logout() {
    if(!$_SESSION['logged_in']) {
      // If not logged in, no point to this
      $this->session->set_flashdata('message_type', 'error');
      $this->session->set_flashdata('message', 'You’re not logged in.');
    } else {
      // Unset all remaining user session data here.
      $session_items = array('user_id', 'name', 'email', 'user_level', 'logged_in');
      $this->session->unset_userdata($session_items);
      $this->session->set_flashdata('message_type', 'success');
      $this->session->set_flashdata('message', 'You’re now logged out!');
      // Redirect user to the main page. We're done!
      redirect('');
    }
  }
}
