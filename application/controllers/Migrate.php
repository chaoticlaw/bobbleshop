<?php
defined('BASEPATH') OR exit('No direct script access allowed!');

class Migrate extends CI_Controller {
  public function index() {
    $this->load->database();
    $this->load->library('migration');

    //After running, check if the migration has completed successfully
    if($this->migration->current() === FALSE) {
      show_error($this->migration->error_string());
    } else {
      $this->session->set_flashdata('message_type', 'success');
      $this->session->set_flashdata('message', 'Database migration successfully completed!');
      redirect('');
    }
  }

  public function backup() {
    // Want to back up? Gotta back up.
    // We specify the tables so we get them in the right order
    // Dumping the SQL view for nicely formatted views does a bad thing so some manual intervention is necessary
    $prefs = array(
      'tables'    =>  array('bobblehead', 'collections', 'inquiries', 'migrations', 'news', 'user', 'view_nice_news'),
      'format'    => 'txt'
    );

    $this->load->dbutil();
    $backup = $this->dbutil->backup($prefs);

    $this->load->helper('file');
    write_file('database.sql', $backup);
    $this->session->set_flashdata('message_type', 'success');
    $this->session->set_flashdata('message', 'Database backed up!');
    redirect('');
  }
}
