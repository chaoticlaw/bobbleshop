<?php
defined('BASEPATH') OR exit('No direct script access allowed!');

class Bobblehead extends CI_Controller {
  public function __construct() {
    parent::__construct();
    $this->load->model('bobblehead_model');
    $this->load->model('collections_model');
    $this->load->helper('flash_bobblehead_notification_helper');
    // Set userdata with the session value for a unique value because CodeIgniter cycles through it
    if(empty($this->session->userdata('cart_session'))) {
      $this->session->set_userdata('cart_session', uniqid(time().session_id(), true));
    }
    if(ENVIRONMENT !== 'production') $this->output->enable_profiler(TRUE);
  }

  // List all bobbleheads
  public function index() {
    // Since all the bobbleheads are in collections, redirect them there
    redirect('collections');
  }

  public function get_all_bobbleheads() {
    $data['bobbleheads'] = $this->bobblehead_model->get_all_bobbleheads();
  }

  // List single bobblehead
  public function item($collection_id = FALSE, $bobblehead_slug = FALSE) {
    $data['bobblehead'] = $this->bobblehead_model->get_bobblehead_by_slug($collection_id, $bobblehead_slug);

    if (empty($data['bobblehead'])) {
      show_404();
    }

    $data['title'] = $data['bobblehead']['name'];

    $this->load->view('templates/header', $data);
    $this->load->view('bobblehead/single_item_view', $data);
    $this->load->view('templates/footer', $data);
  }

  // Create a bobblehead
  public function create() {
    if(!$this->session->userdata('logged_in') && $this->session->userdata('user_level') !== 'admin') {
      $this->load->helper('flash_bobblehead_notification_helper');
      flash_bobble_notification('error', 'You don’t have permission to do that.');
      redirect('');
    }
    $this->load->helper('form');
    $this->load->library('form_validation');

    $data['title'] = 'Create new Bobblehead entry';

    $this->form_validation->set_rules('name', 'Bobblehead Name', 'required|is_unique[bobblehead.name]');
    $this->form_validation->set_rules('description', 'Bobblehead Desciption', 'required');
    $this->form_validation->set_rules('price', 'Price', 'required|decimal');
    $this->form_validation->set_rules('stock', 'Stock', 'required|integer');
    //this->form_validation->set_rules('image', 'Bobblehead image', 'required');

    // Load form data for all collections
    $data['all_collections'] = $this->collections_model->get_all_collections();

    $config['upload_path']    = 'img/product-upload';
    $config['allowed_types']  = 'gif|jpg|jpeg|png';
    // Max upload size in kilobytes
    $config['max_size']       = 2048;
    // Don't preserve the original name
    $config['encrypt_name']   = true;

    $this->load->library('upload', $config);

    if($this->form_validation->run() === FALSE) {
      $this->load->view('templates/header', $data);
      $this->load->view('bobblehead/add_edit_form', $data);
      $this->load->view('templates/footer', $data);
    } else {
      if(!empty($_FILES['image']['name']) && !$this->upload->do_upload('image')) {
        //var_dump($_FILES);
        show_error('Upload failed', 406);
        exit;
      } else {
        $image = $this->upload->data('file_name');
        $this->bobblehead_model->create_bobblehead($image);
        $this->session->set_flashdata('message_type', 'success');
        $this->session->set_flashdata('message', 'Bobblehead created!');
        redirect('collections/');
      }
    }

  }

  // Edit bobblehead
  public function edit($id = NULL) {
    if(!$this->session->userdata('logged_in') && $this->session->userdata('user_level') !== 'admin') {
      $this->session->set_flashdata('message_type,', 'error');
      $this->session->set_flashdata('message', 'You don’t have permission to do that.');
      redirect('');
    }
    $this->load->helper('form');
    $this->load->library('form_validation');

    $data['title'] = 'Edit Bobblehead';

    $this->form_validation->set_rules('name', 'Bobblehead Name', 'required');
    $this->form_validation->set_rules('description', 'Bobblehead Desciption', 'required');

    // Load form data for all collections
    $data['all_collections'] = $this->collections_model->get_all_collections();

    $config['upload_path']    = 'img/product-upload';
    $config['allowed_types']  = 'gif|jpg|jpeg|png';
    $config['max_size']       = 2048;
    $config['encrypt_name']   = true;

    $this->load->library('upload', $config);

    // Get existing data
    $data['bobblehead'] = $this->bobblehead_model->get_bobblehead_by_id($id);

    // Check that it exists
    if (empty($data['bobblehead'])) {
      $this->session->set_flashdata('message_type', 'error');
      $this->session->set_flashdata('message', 'Cannot edit that bobblehead!');
      redirect('bobblehead/create');
    }

    if($this->form_validation->run() === FALSE) {
      $this->load->view('templates/header', $data);
      $this->load->view('bobblehead/add_edit_form', $data);
      $this->load->view('templates/footer', $data);
    } else {
      if(!empty($_FILES['image']['name']) && !$this->upload->do_upload('image')) {
        //var_dump($_FILES);
        show_error('Upload failed', 406);
        exit;
      } else {
        // Use the value of the existing image if we are not uploading a new image
        if ($data['bobblehead']['img_url']) {
          $image = $data['bobblehead']['img_url'];
        } else {
          $image = $this->upload->data('file_name');
        }
        $this->bobblehead_model->update_bobblehead($id, $image);
        if ($this->input->post('destroy')) {
          flash_bobble_notification('success', 'Product was deleted successfully.');
          redirect('collections/');
        } else {
          flash_bobble_notification('success', 'Bobblehead edited!');
          redirect('collections/');
        }
      }
    }
  }

  public function enough_items($bobblehead_id, $amount) {
    $current_stock = $this->bobblehead_model->get_bobblehead_by_id($bobblehead_id)->stock;

    // Check that the amount does not exceed the stock we have
    if ($current_stock > $amount) {
      return true;
    } else {
      $this->form_validation->set_message('enough_items', 'You’ve tried to add too many items!');
      return false;
    }
  }
}
