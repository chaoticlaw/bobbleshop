<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller {
  public function __construct() {
    parent::__construct();
    if(empty($this->session->userdata('cart_session'))) {
      $this->session->set_userdata('cart_session', uniqid(time().session_id(), true));
    }
    
    if(ENVIRONMENT !== 'production') $this->output->enable_profiler(TRUE);
  }

  // By default, load 'views/pages/home.php'
	public function view($page = 'home') {
    if(ENVIRONMENT === 'development') $this->output->enable_profiler(TRUE);
    
    // If we cannot find a specified page, show a 404 error.
    if (!file_exists(APPPATH.'views/pages/'.$page.'.php')) {
      $this->session->set_flashdata('message_type', 'error');
      $this->session->set_flashdata('message', 'That page could not be found');
      redirect('');
    }

    $data['title'] = ucfirst($page); // Sets title variable for the view, and also capitalises the first letter
    

    if ($page === 'home') {
      // Load the list of news items
      // No data to pass in the second argument
      // TRUE here enables a connection to the database
      $this->load->model('news_model', '', TRUE);
      $data['news'] = $this->news_model->get_five_latest_articles();
      $this->load->model('bobblehead_model', '', TRUE);
      $data['featured_bobbleheads'] = $this->bobblehead_model->get_bobbleheads(10, 0);
    }

    // Now load how the page will respond
    // Header, then page, then footer
    $this->load->view('templates/header', $data);
    $this->load->view('pages/'.$page, $data);
    $this->load->view('templates/footer', $data);
	}
}
