<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Posts extends CI_Controller {
  public function __construct() {
    parent::__construct();
    $this->load->model('forum_model');
    $this->load->model('post_model');
    if(empty($this->session->userdata('cart_session'))) {
      $this->session->set_userdata('cart_session', uniqid(time().session_id(), true));
    }
    
    if(ENVIRONMENT !== 'production') $this->output->enable_profiler(TRUE);
  }

  public function make_post($forum_slug = NULL, $topic_id = NULL) {
    $this->load->helper('flash_bobblehead_notification_helper');
    // If the user isn't logged in, send them away
    if (!$this->session->userdata('logged_in')) {
      flash_bobble_notification('error', 'You don’t have permission to do that.');
      redirect('');
    }

    $this->load->helper('form');
    $this->load->library('form_validation');
    $this->form_validation->set_rules('title', 'Thread name', 'required');
    $this->form_validation->set_rules('post_body', 'Body', 'required');

    // Load in form
    if ($this->form_validation->run() === FALSE) {
      $data['title'] = 'New Post';

      $this->load->view('templates/header', $data);
      $this->load->view('posts/create', $data);
      $this->load->view('templates/footer', $data);
    } else {
      // Redirect to new post if new post made
      // Example URL to redirect to:
      // bobblehead.dev/forum/t/example-thread/1
      // Test to make sure forum_id or topic_id are not null
      if (!empty($forum_slug)) {
        // If the forum_slug is set
        $forum = $this->forum_model->get_forum_by_slug($forum_slug);
        $forum_id = $forum->id;
      }
      if (!empty($topic_id)) {
        // If the topic_id is set
        $forum = $this->forum_model->get_forum_by_topic($topic_id);
        $forum_id = $forum->id;
      }
      if (empty($forum)) {
        flash_bobble_notification('error', 'You can’t make a post here.');
        redirect('forum');
      }
      $result = $this->post_model->add_post($forum_id, $topic_id);
      redirect('forum/t/'.$result);
      // or redirect to the thread if it was a reply 
    }
  }

  public function view_topic($topic_id = NULL, $slug = NULL) {
    $this->load->helper('flash_bobblehead_notification_helper');
    if (empty($slug) && empty($topic_id)) {
      redirect('/forum');
    }
    // While the slug makes it easy to read, we don't actually need it so long as the id is right
    $data['thread'] = $this->post_model->get_posts_by_id($topic_id);
    
    // Handle if we don't have any data to load
    if (empty($data['thread'])) {
      flash_bobble_notification('error', 'Thread not found');
      redirect('forum');
    }
    $data['title'] = $data['thread'][0]['title'];
    $data['forum'] = $this->forum_model->get_forum_by_id($data['thread'][0]['forum_id']);
    $this->load->view('templates/header', $data);
    $this->load->view('posts/view_thread', $data);
    $this->load->view('templates/footer', $data);
  }
}
