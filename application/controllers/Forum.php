<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Forum extends CI_Controller {
  public function __construct() {
    parent::__construct();
    $this->load->model('forum_model');
    $this->load->model('post_model');
    if(empty($this->session->userdata('cart_session'))) {
      $this->session->set_userdata('cart_session', uniqid(time().session_id(), true));
    }
    
    if(ENVIRONMENT !== 'production') $this->output->enable_profiler(TRUE);
  }

  public function index() {
    $data['title'] = 'Forum';

    $data['forums'] = $this->forum_model->get_all_forums();
    $data['recent_topics'] = $this->post_model->get_recent_topics(5);

    $this->load->view('templates/header', $data);
    $this->load->view('forum/index', $data);
    $this->load->view('templates/footer', $data);
  }

  public function create_forum() {
    // Require auth, require admin role
    if (!$this->session->userdata('logged_in') && $this->session->userdata('user_level') !== 'admin') {

      $this->load->helper('flash_bobblehead_notification_helper');
      flash_bobble_notification('error', 'You don’t have permission to do that.');
      redirect('/forum');

    } else {

      $this->load->helper('form');
      $this->load->library('form_validation');
      $this->form_validation->set_rules('name', 'Forum Name', 'required');
      $this->form_validation->set_rules('slug', 'URL slug', 'is_unique[forum.slug]');


      $data['title'] = 'Create Forum';
      if ($this->form_validation->run() === FALSE) {
        $this->load->view('templates/header', $data);
        $this->load->view('forum/create', $data);
        $this->load->view('templates/footer', $data);
      } else {
        redirect('forum/f/'.$this->forum_model->add_forum());
      }
    }
  }

  public function edit_forum($id = NULL) {
    // Require auth, require admin role
    if (!$this->session->userdata('logged_in') && $this->session->userdata('user_level') !== 'admin') {

      $this->load->helper('flash_bobblehead_notification_helper');
      flash_bobble_notification('error', 'You don’t have permission to do that.');
      redirect('/forum');

    } else {
      $this->load->helper('form');
      $this->load->library('form_validation');
      $this->form_validation->set_rules('name', 'Forum Name', 'required');
      $this->form_validation->set_rules('slug', 'URL slug', 'callback_forum_unique[forum.slug]');
      $data['forum'] = $this->forum_model->get_forum_by_id($id);

      if (empty($data['forum'])) {
        $this->load->helper('flash_bobblehead_notification_helper');
        flash_bobble_notification('error', 'We couldn’t load that.');
        redirect('forum/manage');
      }

      // If the form has been submitted, do something
      if ($this->form_validation->run() === FALSE) {
        // Else we show the form
        $data['title'] = 'Edit Forum';
        $this->load->view('templates/header', $data);
        $this->load->view('forum/edit', $data);
        $this->load->view('templates/footer', $data);
      } else {
        // Update the forum
        $this->forum_model->update_forum($id);
        $this->load->helper('flash_bobblehead_notification_helper');
        if ($this->input->post('destroy')) {
          flash_bobble_notification('success', 'Forum deleted.');
          redirect('forum/manage');
        } else {
          flash_bobble_notification('success', 'Forum successfully updated!');
          redirect('forum/edit/'.$id);
        }
      }
    }
  }

  public function forum_unique($string, $id) {
    $current_forum = $this->forum_model->get_forum_by_id($id);
    $check = !empty($this->forum_model->get_forum_by_slug($string));

    if ($string !== $current_forum['slug'] || !$check) {
      return TRUE;
    } else {
      $this->form_validation->set_message('forum_unique', 'The forum slug is not unique');
      return FALSE;
    }
  }

  public function view_forum($slug = NULL) {
    $this->load->helper('flash_bobblehead_notification_helper');
    $data['forum'] = $this->forum_model->get_forum_by_slug($slug);
    // If no data is found, redirect
    if (empty($data['forum'])) {
      flash_bobble_notification('error', 'We couldn’t find that forum.');
      redirect('/forum');
    }

    // Get topics
    $data['topics'] = $this->post_model->get_topics_by_category($data['forum']->id);
    //$this->output->append_output(var_dump($data['topics']));
    $data['title'] = $data['forum']->name;
    $data['description'] = $data['forum']->description;
    // To count the number of replies, load the post_model into the view
    $data['post_model'] = $this->post_model;
    $this->load->view('templates/header', $data);
    $this->load->view('forum/view', $data);
    $this->load->view('templates/footer', $data);
  }

  public function manage() {
    if (!$this->session->userdata('logged_in') && $this->session->userdata('user_level') !== 'admin') {
      $this->load->helper('flash_bobblehead_notification_helper');
      flash_bobble_notification('error', 'You don’t have permission to do that.');
      redirect('/forum');
    } else {
      // Load forums
      $data['forums'] = $this->forum_model->get_all_forums();

      $data['title'] = 'Manage Forums';
      $this->load->view('templates/header', $data);
      $this->load->view('forum/manage', $data);
      $this->load->view('templates/footer', $data);
    }
  }
}
