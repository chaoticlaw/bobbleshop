<?php
defined('BASEPATH') OR exit('No direct script access allowed!');

class Custom_Bobble extends CI_Controller {
  public function __construct() {
    parent::__construct();
    $this->load->model('user_model');
    $this->load->model('bobblehead_model');
    $this->load->model('collections_model');
    $this->load->model('custom_bobble_model'); 
    if(empty($this->session->userdata('cart_session'))) {
      $this->session->set_userdata('cart_session', uniqid(time().session_id(), true));
    }
    
    if(ENVIRONMENT !== 'production') $this->output->enable_profiler(TRUE);
  }


  // Primary form here
  public function create() {
    $this->load->helper('form');
    $this->load->library('form_validation');

    $data['title'] = 'Inquire about our Custom Bobbleheads';
    $data['user'] = $this->user_model->get_user_by_id($this->session->userdata('user_id'));

    $this->form_validation->set_rules('user_fullname', 'Full name', 'required');
    $this->form_validation->set_rules('user_email', 'E-mail', 'required|valid_email');
    $this->form_validation->set_rules('description', 'Description', 'required');
    $this->form_validation->set_rules('bobblehead_id', 'Bobblehead Style', 'required');

    $data['all_bobbleheads'] = $this->bobblehead_model->get_all_bobbleheads();

    $config['upload_path']    = 'img/custom-bobbleheads/';
    $config['allowed_types']  = 'gif|jpg|jpeg|png';
    $config['max_size']       = 2048;
    $config['encrypt_name']   = true;

    $this->load->library('upload', $config);

    if($this->form_validation->run() === FALSE) {
      $this->load->view('templates/header', $data);
      $this->load->view('custom/inquiry_form', $data);
      $this->load->view('templates/footer', $data);
    } else {
      if(!empty($_FILES['image']['name']) && !$this->upload->do_upload('image')) {
        //var_dump($_FILES);
        show_error('Upload failed', 406);
        exit;
      } else {
        $image = $this->upload->data('file_name');
        $this->custom_bobble_model->create($image);
        $this->session->set_flashdata('message_type', 'success');
        $this->session->set_flashdata('message', 'Inquiry Sent!');
        redirect('');
      }
    }
  }

  // List all requests made by a user
  public function review($offset_page = 1) {
    // If our user is an admin, we can show all the records
    // Otherwise if they're a regular user, we show just their own submissions
    // And if not any of those, send them back to the login area
    // Work through them in the reverse order

    $data['title'] = 'Review submitted inquiries';

    if (!$this->session->userdata('logged_in')) {
      $this->session->set_flashdata('message_type', 'error');
      $this->session->set_flashdata('message', 'You must be logged in to do that');
      redirect('user/login');
    } elseif ($this->session->userdata('user_level') !== 'admin') {
      $data['submissions'] = $this->custom_bobble_model->get_inquiries_by_user_id($this->session->userdata('user_id'), 10, $offset_page - 1);
      $data['submission_count'] = $this->custom_bobble_model->count_inquiries_by_user_id($this->session->userdata('user_id'));
    } else {
      $data['submissions'] = $this->custom_bobble_model->get_inquiries(10, $offset_page - 1);
      $data['submission_count'] = $this->custom_bobble_model->count_all_inquiries();
    }
    // Load data in
    $pagination_conf['base_url'] = base_url('custom/list/');
    $pagination_conf['total_rows'] = $data['submission_count'];
    $pagination_conf['per_page'] = 10;
    $pagination_conf['use_page_numbers'] = true;
    $pagination_conf['full_tag_open'] = '<ul class="pagination">';
    $pagination_conf['full_tag_close'] = '</ul>';
    $pagination_conf['first_tag_open'] = '<li>';
    $pagination_conf['first_tag_close'] = '</li>';
    $pagination_conf['last_tag_open'] = '<li>';
    $pagination_conf['last_tag_close'] = '</li>';
    $pagination_conf['next_tag_open'] = '<li>';
    $pagination_conf['next_tag_close'] = '</li>';
    $pagination_conf['prev_tag_open'] = '<li>';
    $pagination_conf['prev_tag_close'] = '</li>';
    $pagination_conf['cur_tag_open'] = '<li class="active"><a href="#">';
    $pagination_conf['cur_tag_close'] = '</a></li>';
    $pagination_conf['num_tag_open'] = '<li>';
    $pagination_conf['num_tag_close'] = '</li>';

    $this->load->library('pagination');
    $this->pagination->initialize($pagination_conf);

    $this->load->view('templates/header', $data);
    $this->load->view('custom/inquiry_list', $data);
    $this->load->view('templates/footer', $data);
  }

  // List the details of a bobblehead request
  // At this point in time, this function is only for review, and details cannot be edited.
  public function details($inquiry_id = NULL) {
    // Grab the details so we can compare the owner of the request to the user accessing it
    $data['inquiry'] = $this->custom_bobble_model->get_inquiry_by_id($inquiry_id);
    $data['title'] = 'Inquiry Details';

    // Check if they're logged in at all
    if (!$this->session->userdata('logged_in')) {
      $this->session->set_flashdata('message_type', 'error');
      $this->session->set_flashdata('message', 'You must be logged in to do that');
      redirect('user/login');
    } elseif ($this->session->userdata('user_level') !== 'admin' && $this->session->userdata('user_id') !== $data['inquiry']['user_id']) {
      // Check if user is not an admin and owner of the request
      $this->session->set_flashdata('message_type', 'error');
      $this->session->set_flashdata('message', 'You do not have permission to do that');
      redirect('custom/list');
    } else {
      // Load data in
      $this->load->view('templates/header', $data);
      $this->load->view('custom/inquiry_details', $data);
      $this->load->view('templates/footer', $data);
    }
  }
}
