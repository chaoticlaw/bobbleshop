<?php
defined('BASEPATH') OR exit('No direct script access allowed!');
// For the current page, we grab part of the url.
$current_page = $this->uri->segment(1);
$this->load->helper('form');

Switch(ENVIRONMENT) {
  case('development'):
    $navbar_type = "navbar-default";
  break;
  default:
    $navbar_type = "navbar-inverse";
  break;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Bobbleshop: Custom Bobbleheads - <?=$title?></title>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css" media="screen">
  <link href="https://fonts.googleapis.com/css?family=Raleway:700" rel="stylesheet"> 
  <link rel="stylesheet" href="<?=base_url('css').'/main.css'?>">

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js" integrity="sha384-xBuQ/xzmlsLoJpyjoggmTEz8OWUFM0/RC5BsqQBDX2v5cMvDHcMakNTNrHIW2I5f" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  <script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script>
  <script src="https://use.fontawesome.com/97c6653b79.js"></script>

  <!-- Code for favicons -->
  <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
  <link rel="manifest" href="/manifest.json">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="theme-color" content="#ffffff">
</head>
<body>
<nav class="navbar <?=$navbar_type?>" role="navigation">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a href="<?=base_url()?>" class="navbar-brand">
        <img src="<?=base_url().'img/bobblehead-logo-square-24.png'?>" alt="Bobbleshop: The latest Bobblehead designs">
      </a>
    </div>
    <div class="collapse navbar-collapse" id="menu">
      <!--<ul class="nav navbar-nav navbar-left">
        <li><a href="https://twitter.com/Bobbleshop">
          <i class="fa fa-twitter fa-lg fa-fw" aria-hidden="true"></i> <span class="visible-xs-inline">Twitter</span>
        </a></li>
        <li><a href="https://facebook.com/Bobbleshop">
          <i class="fa fa-facebook fa-lg fa-fw" aria-hidden="true"></i> <span class="visible-xs-inline">Facebook</span>
        </a></li>
      </ul>-->
      <nav>
        <ul class="nav navbar-nav navbar-left">
          <li <?php echo $current_page === 'custom' ? 'class="active"' : '' ?>><a href="<?=base_url('custom')?>">Custom<span class="hidden-sm"> Made</span></a></li>
          <li <?php echo $current_page === 'collections' ? 'class="active"' : '' ?>><a href="<?=base_url('collections')?>">Collections</a></li>
          <li <?php echo $current_page === 'forum' ? 'class="active"' : '' ?>><a href="<?=base_url('forum')?>">Forum</a></li>
          <li <?php echo $current_page === 'about' ? 'class="active"' : '' ?>><a href="<?=base_url('about')?>">About</a></li>
          <li <?php echo $current_page === 'contact' ? 'class="active"' : '' ?>><a href="<?=base_url('contact')?>">Contact<span class="hidden-sm"> Us</span></a></li>
        </ul>
      </nav>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="<?=base_url('cart')?>"><i class="fa fa-shopping-cart fa-lg fa-fw" aria-hidden="true"></i> Cart</a></li>
      <?php if ($this->session->userdata('logged_in')):?>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
          <?php if ($this->session->userdata('user_level') === 'admin'): ?>
              <i class="fa fa-shield fa-lg fa-fw" aria-hidden="true"></i> 
            <?php else: ?>
              <i class="fa fa-user fa-lg fa-fw" aria-hidden="true"></i> 
          <?php endif ?>
          <?=$this->session->userdata('name')?> <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<?=base_url('user/')?>">User Menu</a></li>
            <?php if ($this->session->userdata('user_level') === 'admin'): ?>
            <li class="divider" role="separator"></li>
            <li><a href="<?=base_url('collections/create')?>">Create Collection</a></li>
            <li><a href="<?=base_url('bobblehead/create')?>">Create Bobblehead</a></li>
            <li class="divider" role="separator"></li>
            <?php endif ?>
            <li><a href="<?=base_url('user/logout')?>"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a></li>
          </ul>
        </li>
      <?php else: ?>
        <li><a href="<?=base_url('user/login')?>" class="">Log in</a></li>
        <li><a href="<?=base_url('user/register')?>" class="">Register</a></li>
      <?php endif; ?>
      </ul>
      <?=form_open('search', 'class="navbar-form navbar-right"')?>
        <div class="form-group">
          <input type="text" class="form-control" name="search_term" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default"><i class="fa fa-search" aria-label="search"></i></button>
      </form>
    </div>
  </div>
</nav>
<div class="container">
  <?php if ($this->session->userdata('message')):?>
  <div class="alert
    alert-<?php switch($this->session->userdata('message_type')) {
      case 'error':
        echo 'danger';
        break;
      case 'success':
        echo 'success';
        break;
      }
  ?> alert-dismissible" role="alert">
    <button type="button" class="close" aria-label="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
    <h4><?=ucfirst($this->session->userdata('message_type'))?></h4>
    <p><?php echo $this->session->userdata('message'); ?></p>
  </div>
  <?php endif; ?>
