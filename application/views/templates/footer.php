</div>
<footer>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-md-4">
        <h1>Copyright</h1>
        <p>&copy; 2017 Bobbleshop. All Rights Reserved.</p>
        <p>Site design by Q. M. Nguyen</p>
      </div>
      <div class="col-xs-12 col-md-4">
        <h1>Other Links</h1>
        <ul class="list-group">
          <li class="list-group-item"><a href="https://twitter.com/Bobbleshop">
            <i class="fa fa-twitter fa-lg fa-fw" aria-hidden="true"></i> Twitter
          </a></li>
          <li class="list-group-item"><a href="https://facebook.com/Bobbleshop"><i class="fa fa-facebook fa-lg fa-fw" aria-hidden="true"></i> Facebook</a></li>
          <li class="list-group-item"><a href="<?=base_url('terms')?>">Terms of Service</a></li>
          <li class="list-group-item"><a href="<?=base_url('privacy-policy')?>">Privacy Policy</a></li>
        </ul>
      </div>
      <nav class="col-xs-12 col-md-4">
        <h1>Navigation</h1>
        <ul class="list-group">
        <?php
        $current_page = $this->uri->segment(1);
        ?>
          <li class="list-group-item">
            <a href="<?=base_url('custom')?>" class="<?=$current_page === 'custom' ? 'active' : '' ?>">Custom Made</a>
          </li>
          <li class="list-group-item">
            <a href="<?=base_url('collections')?>" class="<?=$current_page === 'collections' ? 'active' : '' ?>">Collections</a>
          </li>
          <li class="list-group-item">
            <a href="<?=base_url('about')?>" class="<?=$current_page === 'about' ? 'active' : '' ?>">About</a>
          </li>
          <li class="list-group-item">
            <a href="<?=base_url('contact')?>" class="<?=$current_page === 'contact' ? 'active' : '' ?>">Contact Us</a>
          </li>
        </ul>
      </nav>
    </div>
  </div>
</footer>
<script type="text/javascript">
  $(function() {
    // Setup for Ajax call
    function make_search(query) {
      var request_fields = {'search_term': query}

      $.ajax({
        type: "POST",
        url: '<?= base_url('search/json')?>',
        data: request_fields,
        timeout: 3000,
        success: function(dataBack) {
          if ($('#search_results').length > 0) {
            $('#search_results').remove();
          }
          $('#menu').append('<ul id="search_results" class="search_results"></ul>');
          $.each(dataBack, function(index, element) {
            $('#search_results').append('<li><a href="<?= base_url('collections/') ?>'
              + element.collection_slug
              + '/item/'
              + element.slug
              + '">'
              + element.name
              + '</a></li>');
          });
        },
        error: function() {
          console.log('An error occurred when trying to retrieve search results via JSON');
        }
      });
    }

    var typing_timer;
    var typing_interval = 750;
    var search_bar = $('input[name="search_term"]');

    search_bar.on('keyup', function() {
      clearTimeout(typing_timer);
      if (search_bar.val()) {
        typing_timer = setTimeout(function() { make_search(search_bar.val()) }, typing_interval);
      }
      // If there's nothing
      if (!search_bar.val()) {
        $('#search_results').remove();
      }
    });
  });
</script>
</body>
</html>
