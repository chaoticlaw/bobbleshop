<?php
defined('BASEPATH') OR exit('No direct script access allowed!');
?>
<main>
  <div class="row">
    <div class="col-xs-12 col-md-6">
      <h1>Contact Us</h1>
      <div class="panel panel-warning">
        <div class="panel-heading">
          <h3 class="panel-title">Alert</h3>
        </div>
        <div class="panel-body">
          <p>This contact form currently does not submit.</p>
        </div>
      </div>
      <form action="#" method="post">
        <div class="form-group">
          <label for="form_fullname" class="control-label">Full Name</label>
          <input type="text" name="name" id="form_fullname" class="form-control" value="<?=!empty($this->session->userdata('name')) ? $this->session->userdata('name') : ''?>">
        </div>
        <div class="form-group">
          <label for="form_email" class="control-label">E-mail</label>
          <div class="input-group">
            <div class="input-group-addon"><i class="fa fa-envelope" aria-hidden="true"></i></div>
            <input type="email" name="email" id="form_email" class="form-control" value="<?=!empty($this->session->userdata('email')) ? $this->session->userdata('email') : ''?>">
          </div>
        </div>
        <div class="form-group">
          <label for="form_message" class="control-label">Message</label>
          <textarea name="message" id="form_message" class="form-control"></textarea>
        </div>
        <div class="form-group">
          <button type="submit" name="submit" class="btn btn-default" onclick="preventDefault(); return false;">Send Message</button>
        </div>
      </form>
    </div>
    <div class="col-xs-12 col-md-6">
      <h1>Where we are</h1>
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d828.0805151773766!2d151.20042978815144!3d-33.881358485408114!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6b12ae25cfc4daef%3A0x46ad564960013c63!2sItem+International!5e0!3m2!1sen!2sau!4v1496322980145" id="contact-map"></iframe>
    </div>
  </div>
</main>
