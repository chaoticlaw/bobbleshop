<?php
defined('BASEPATH') OR exit('No direct script access allowed!');
?>
<main>
  <section class="row">
    <div class="col-xs-12">
      <h1>About</h1>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempora, itaque cum voluptas repudiandae laborum voluptatum, impedit nam nisi, porro illo dolores vitae vel harum assumenda alias quos. Quidem, tempore, sit.</p>
      <p>Tempora, voluptate numquam, sed, doloribus ut maiores, reprehenderit earum nobis repellat ratione iste sunt dolore distinctio. Quas, atque. Dolores fuga, sed velit qui aut, sequi iure doloribus harum beatae aliquam!</p>
      <p>Sed officia quam, porro sapiente saepe explicabo illo animi quibusdam impedit minima officiis eveniet iste recusandae aut autem! Cumque voluptatum dignissimos, quos voluptate placeat. Atque laboriosam voluptate iste, ad impedit.</p>
      <p>Ipsam vel cumque ullam adipisci asperiores incidunt vero quos, culpa eligendi accusamus voluptate. Nesciunt dignissimos voluptatibus numquam voluptatem ullam assumenda labore ad voluptas aliquid! Distinctio cupiditate consectetur explicabo alias, iusto.</p>
      <p>Deleniti omnis explicabo harum odio, amet. Repellat unde quidem rerum ducimus sed eos, magni itaque tempore! Esse illo ratione consectetur, illum dicta atque. Facere iusto sapiente, fuga esse culpa odit.</p>
      <p>Ratione beatae iure consequuntur cum sint maxime excepturi eius, veritatis ex. Minus impedit corrupti perferendis quae repudiandae velit, dolores deleniti. Unde iste in voluptatum quam, dicta enim odio provident commodi.</p>
      <p>Unde accusamus laborum iure tempore magni, repellendus rem tempora animi cupiditate odio, qui quia. Impedit quisquam saepe laborum eveniet nemo, cum voluptatem totam maxime doloribus nisi. Ullam minus, quod repudiandae!</p>
      <p>Modi deleniti, voluptatibus. Facilis provident non, impedit dolore, nulla porro at harum. Quod repudiandae in obcaecati quam perspiciatis laborum voluptatum sed, alias debitis cupiditate nulla neque illum quia odit dignissimos.</p>
    </div>
  </section>
</main>
