<?php
defined('BASEPATH') OR exit('No direct script access allowed!');
?>
<main>
  <section class="row">
    <div class="col-xs-12">
      <img src="<?=base_url('img').'/bobblehead-logo-400.png'?>" width="400" height="137" alt="Bobbleshop - The latest Bobblehead Designs" class="img-responsive">

      <div class="banner">
        <div class="carousel-cell banner b1" style="background-image: url('<?=base_url('img/site-assets/')."banner-1.jpg"?>');">
          <div class="vcentre">
            <h1>Not sure what you want to get?</h1>
            <p>Send us a portrait, we can make them</p>
          </div>
        </div>
        <div class="carousel-cell banner b2" style="background-image: url('<?=base_url('img/site-assets/')."banner-2.jpg"?>');">
          <div class="vcentre">
            <h1>Quality Guaranteed</h1>
            <p>So strong even a truck couldn't crush it</p>
          </div>
        </div>
        <div class="carousel-cell banner b3" style="background-image: url('<?=base_url('img/site-assets/')."banner-3.jpg"?>');">
          <div class="vcentre">
            <h1>Experts in the Industry</h1>
            <p>Need Bobbleheads for your business? We can do it.</p>
          </div>
        </div>
        <div class="carousel-cell banner b4" style="background-image: url('<?=base_url('img/site-assets/')."banner-4.jpg"?>');">
          <div class="vcentre">
            <h1>It'll make you smile</h1>
            <p>If it doesn't, you shouldn't have bought it</p>
          </div>
        </div>
      </div>

      <canvas id="custom-made" width="1280" height="400">
        <h1>Make your own custom bobblehead!</h1>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempore recusandae, corrupti laboriosam cum aliquid quidem adipisci nam animi minima explicabo asperiores dolor necessitatibus aliquam nihil aspernatur, velit expedita nulla atque.</p>
        <a href="<?=base_url('custom')?>" class="btn btn-primary">Make your own</a>
      </canvas>
    </div>
  </section>
  <section class="row">
    <section class="col-lg-6 col-md-6 col-xs-12">
      <h1>Featured Bobbleheads</h1>
      <div class="featured">
      <?php foreach ($featured_bobbleheads as $bobblehead): ?>
        <div class="thumbnail">
        <?php
        // This grabs the first paragraph
        // This wouldn't work if we didn't use paragraph tags in the description
          $start = strpos($bobblehead['description'], '<p>');
          $end = strpos($bobblehead['description'], '</p>', $start);
          $paragraph = substr($bobblehead['description'], $start, $end-$start+4);
        ?>
          <img src="<?=base_url('img/product-upload').'/'.$bobblehead['img_url']?>" alt="<?=$bobblehead['name']?>" class="img-responsive" data-toggle="tooltip" data-placement="bottom" title="<?=htmlspecialchars(strip_tags($paragraph))?>">
          <div class="caption">
            <h3><?=$bobblehead['name']?></h3>
            <p><a href="<?=base_url('collections').'/'.$bobblehead['collection_slug'].'/item/'.$bobblehead['slug']?>" class="btn btn-primary">Look</a></p>
          </div>
        </div>
      <?php endforeach ?>
      </div>
    </section>
    <section class="col-lg-6 col-md-6 col-xs-12" id="news">
      <h1>Latest News</h1>
      <?php foreach ($news as $news_item):?>
        <div class="row">
          <div class="col-xs-12">
            <h2><?php echo $news_item['news_title']?> <small><?php echo date("jS F, Y", strtotime($news_item['news_date'])); ?></small> <span class="pull-right caret"></span></h2>
            <article>
            <?php
              $start = strpos($news_item['news_content'], '<p>');
              $end = strpos($news_item['news_content'], '</p>', $start);
              $paragraph = substr($news_item['news_content'], $start, $end-$start+4);
            ?>
              <section class="news_content"><?=$paragraph?></section>
              <p><a href="<?=base_url('news').'/'.$news_item['post_year'].'/'.$news_item['post_month'].'/'.$news_item['slug']?>">Read more</a></p>
            </article>
          </div>
        </div>
      <?php endforeach ?>
      <a href="<?=base_url('news')?>" class="button">Archive</a>
    </section>
  </section>
</main>
<!-- Add Easel and Tween to make working with canvas easier -->
<script src="https://code.createjs.com/easeljs-0.8.2.min.js"></script>
<script src="https://code.createjs.com/tweenjs-0.6.2.min.js"></script>
<script>
  $(function() {
    // Create our canvas (HTML requirements)
    var custom = new createjs.Stage("custom-made");

    var photo = new createjs.Bitmap("<?=base_url('img/site-assets/custom-bobblehead-photo.jpg')?>");
    var circle_mask = new createjs.Shape();
    circle_mask.graphics.beginFill("#000").drawCircle(200, 0, 200);
    circle_mask.x = 0;
    circle_mask.y = 200;
    //photo.alpha = 0;
    photo.mask = circle_mask;
    var photo_comp = new createjs.Container();
    photo_comp.x = 1280;
    photo_comp.y = 100;
    photo_comp.scaleX = 0.75;
    photo_comp.scaleY = 0.75;
    photo_comp.addChild(photo);
    custom.addChild(photo_comp);

    createjs.Tween.get(photo_comp, {loop: false})
    .to({x: 640 - 200}, 1500, createjs.Ease.getPowInOut(4));

    var circle = new createjs.Shape();
    circle.graphics.beginFill("#ddd").drawCircle(0, 0, 50);
    circle.x = 1280;
    circle.alpha = 0;
    circle.y = 300;
    custom.addChild(circle);
    
    createjs.Tween.get(circle, {loop: true})
    .to({ alpha: 1, x: 0 + 50}, 1000, createjs.Ease.getPowInOut(4))
    .to({ alpha: 1, x: 0 + 50}, 3000, createjs.Ease.getPowInOut(4))
    .to({ alpha: 0, x: 1280}, 1000, createjs.Ease.getPowInOut(2));

    

    var heading = new createjs.Text('Create a custom bobblehead!', "bold 64px Raleway", "#8f2878");
    heading.x = 1280;
    heading.textAlign = 'right';
    custom.addChild(heading);

    // To create the button, create rectangle, then text, then stick them together
    var btn_bg = new createjs.Shape();
    btn_bg.name = 'background';
    btn_bg.graphics.beginFill("#8f2878").drawRoundRect(0, 0, 400, 100, 10);

    var btn_txt = new createjs.Text('Make one now!', '48px Raleway', "#fff");
    btn_txt.name = 'text';
    btn_txt.textAlign = 'center';
    btn_txt.textBaseline = 'middle';
    btn_txt.x = 400/2;
    btn_txt.y = 100/2;

    var button = new createjs.Container();
    button.name = 'button';
    button.x = 1280 - 400;
    button.y = 400 - 100 - 50;
    button.addChild(btn_bg, btn_txt);
    button.addEventListener('click', function(e) {
      window.location.href = '<?=base_url('custom');?>';
    });
    custom.addChild(button);

    createjs.Ticker.setFPS(60);
    createjs.Ticker.addEventListener('tick', custom);
    /*
    6. Add functionality to the index.htm page. When the user moves the mouse over one of the images, a brief description of the bobblehead should appear in the form of a rich tooltip. Moving the mouse off the image should make this description disappear.
    */

    $('[data-toggle="tooltip"]').tooltip();

    /*
    10. Create an image gallery that allows the user to move through a selection of 'recommended' bobbleheads. The user should be able to move backwards and forward through the selection.
    */

    $('.banner').flickity({
      wrapAround: true,
      imagesLoaded: true,
      autoPlay: true,
      cellSelector: '.carousel-cell',
      prevNextButtons: false,
      pageDots: false,
      setGallerySize: true
    });

    $('.featured').flickity({
      wrapAround: true,
      imagesLoaded: true,
      pageDots: false,
      setGallerySize: true
    });

    /*
    11. Modify the index.htm page, so that the news items are listed by headline only. When a headline is selected, a story should open underneath it (moving all the other headlines down).
    */

    $('#news').find('article').hide().end().find('h2').click(function() {
      $(this).next().slideToggle();
    });
  });

  
</script>
