<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<main>
  <div class="row">
    <div class="col-xs-12">
      <h1>Privacy Policy</h1>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique natus minima cumque voluptates quaerat illum quo, illo tempore, quos ad praesentium ab, facere rem optio exercitationem sed hic, eius nihil!</p>
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12 col-md-8">
      <h2>Full Legal</h2>
    </div>
    <div class="col-xs-12 col-md-4">
      <h2>Summary</h2>
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12 col-md-8">
      <h3>Section 1</h3>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id harum adipisci est quis, soluta, numquam? Perferendis perspiciatis dicta veniam commodi voluptas aliquam, aut, architecto optio qui saepe praesentium soluta laudantium.</p>
      <p>Beatae nemo fugiat, quod autem. Sequi id aspernatur nostrum autem quaerat deleniti accusamus eius est porro alias! Expedita, obcaecati, saepe. Dolore quis, repellat sapiente libero repudiandae ratione amet harum facere.</p>
      <p>Harum ducimus, perspiciatis perferendis veniam natus necessitatibus asperiores doloremque minus, officiis rem molestiae porro maxime recusandae dolor quod! Cupiditate quas qui distinctio pariatur ab doloremque et quidem architecto est adipisci!</p>
    </div>
    <div class="col-xs-12 col-md-4">
      <h3>Summary</h3>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempora perspiciatis illo et in enim totam odio voluptates sint recusandae id maiores hic magni fugiat nesciunt, aut. Commodi ea tenetur expedita!</p>
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12 col-md-8">
      <h3>Section 2</h3>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id harum adipisci est quis, soluta, numquam? Perferendis perspiciatis dicta veniam commodi voluptas aliquam, aut, architecto optio qui saepe praesentium soluta laudantium.</p>
      <p>Beatae nemo fugiat, quod autem. Sequi id aspernatur nostrum autem quaerat deleniti accusamus eius est porro alias! Expedita, obcaecati, saepe. Dolore quis, repellat sapiente libero repudiandae ratione amet harum facere.</p>
      <p>Harum ducimus, perspiciatis perferendis veniam natus necessitatibus asperiores doloremque minus, officiis rem molestiae porro maxime recusandae dolor quod! Cupiditate quas qui distinctio pariatur ab doloremque et quidem architecto est adipisci!</p>
    </div>
    <div class="col-xs-12 col-md-4">
      <h3>Summary</h3>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempora perspiciatis illo et in enim totam odio voluptates sint recusandae id maiores hic magni fugiat nesciunt, aut. Commodi ea tenetur expedita!</p>
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12 col-md-8">
      <h3>Section 3</h3>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id harum adipisci est quis, soluta, numquam? Perferendis perspiciatis dicta veniam commodi voluptas aliquam, aut, architecto optio qui saepe praesentium soluta laudantium.</p>
      <p>Beatae nemo fugiat, quod autem. Sequi id aspernatur nostrum autem quaerat deleniti accusamus eius est porro alias! Expedita, obcaecati, saepe. Dolore quis, repellat sapiente libero repudiandae ratione amet harum facere.</p>
      <p>Harum ducimus, perspiciatis perferendis veniam natus necessitatibus asperiores doloremque minus, officiis rem molestiae porro maxime recusandae dolor quod! Cupiditate quas qui distinctio pariatur ab doloremque et quidem architecto est adipisci!</p>
    </div>
    <div class="col-xs-12 col-md-4">
      <h3>Summary</h3>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempora perspiciatis illo et in enim totam odio voluptates sint recusandae id maiores hic magni fugiat nesciunt, aut. Commodi ea tenetur expedita!</p>
    </div>
  </div>
</main>
