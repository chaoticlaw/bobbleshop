<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<main>
  <section class="row">
    <div class="col-xs-12">
      <h1>Legal</h1>
      <p>Here are the terms of our website.</p>
    </div>
  </section>
  <section class="row">
    <div class="col-xs-12">
      <h1>Section 1</h1>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam sed repellat nesciunt unde consequatur sapiente eum neque sint laboriosam sunt. Exercitationem possimus voluptatem dolor voluptas harum nostrum, mollitia debitis. Labore.</p>
      <p>Officia, placeat, aliquam sapiente ducimus saepe molestias, nemo adipisci praesentium harum laudantium perspiciatis ipsum obcaecati quia dignissimos itaque quidem. Pariatur vero minus quidem sapiente molestias dolore, doloribus necessitatibus ex beatae!</p>
      <p>Odit assumenda quam beatae est ea rem aliquam, modi ad ipsum officia tempora, distinctio neque placeat cupiditate, quis fugiat reprehenderit, fugit architecto obcaecati deleniti pariatur. Architecto ea veniam doloribus error!</p>
      <p>Ducimus perferendis repellat eius. Obcaecati enim praesentium doloremque odit! Esse odio, modi, quos ab ipsam error inventore quibusdam nesciunt eius quis enim saepe optio impedit ipsum. Laboriosam quam, laborum atque!</p>
      <h1>Section 2</h1>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt officia veniam odit optio doloremque voluptatum est totam, qui amet dolore officiis soluta nostrum voluptatem accusamus, molestiae, laborum minima ducimus assumenda.</p>
      <p>Consectetur iusto quas at tempore labore, veritatis cupiditate rem unde id natus illum explicabo esse distinctio reprehenderit velit repellendus saepe. Deserunt nulla doloremque temporibus blanditiis ipsam, praesentium cumque ut adipisci.</p>
      <p>Ea provident laboriosam, necessitatibus aliquid sunt, quis obcaecati ab unde soluta accusamus esse recusandae vel non similique aut rerum assumenda cum ducimus deleniti. Quae omnis labore ex at, doloremque repellat!</p>
      <p>Laborum voluptates fugiat facere repellat recusandae sunt in consequuntur natus, ipsam, quia cupiditate officiis quo. At eligendi, dicta iusto praesentium voluptas. Dolor sequi, rem eius harum explicabo soluta adipisci iure.</p>
      <h1>Section 3</h1>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus, aliquam? Beatae ad rem dignissimos eligendi vel ipsam commodi explicabo maxime iusto libero fugiat perspiciatis suscipit, debitis alias eveniet aperiam blanditiis.</p>
      <p>Vitae quis nostrum nam libero similique deserunt sit. Distinctio cumque repellendus consequuntur ipsam soluta suscipit eius est, voluptate modi eligendi magnam possimus et non, dolore maiores, obcaecati sed autem! Ex!</p>
      <p>Porro, aspernatur. Placeat consequatur ab accusantium at doloremque sequi necessitatibus voluptas voluptatum mollitia minus cumque eaque, quod inventore, amet dolorem iste delectus magni. Voluptatem facilis quas commodi molestias modi veritatis.</p>
      <p>Quidem itaque, consequatur quaerat, obcaecati ducimus mollitia pariatur porro earum voluptates a odit doloremque quod ab laboriosam sit suscipit. Inventore illum commodi explicabo tempora eos eaque accusantium maxime temporibus ab!</p>
      <h1>Section 4</h1>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse voluptatum doloremque repellendus! Cumque quod qui porro, beatae fuga reiciendis pariatur a vel expedita voluptate sunt, quis. Assumenda harum ea deserunt.</p>
      <p>Possimus odio molestiae commodi necessitatibus impedit debitis expedita quas. Vel placeat cumque corporis aliquam dolorem laboriosam, illo modi deserunt amet, et animi veritatis voluptatibus. Quo recusandae, minus? Atque, porro modi.</p>
      <p>Esse tempora facilis qui suscipit eaque iusto reprehenderit ab sapiente. Dolor numquam veniam quos! Perspiciatis ipsum quos nemo explicabo soluta, laboriosam, aliquid, hic voluptatum harum earum itaque veniam asperiores aut.</p>
      <p>Amet, aut. Quos illo officia aspernatur vel, velit aliquid, reprehenderit obcaecati deleniti asperiores, voluptatibus porro accusantium modi amet atque. Minima soluta ratione enim nisi, error officiis eligendi, facere obcaecati non.</p>
    </div>
  </section>
</main>
