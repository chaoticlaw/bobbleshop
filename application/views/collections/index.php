<?php
defined('BASEPATH') OR exit('No direct script access allowed!');
?>
<main>
  <div class="row">
    <section class="col-xs-12">
      <h1><?=$title?></h1>
      <p><?=$description?></p>
    </section>
  </div>
  <div class="row">
    <div class="col-xs-12 col-md-4 col-lg-3">
      <div class="list-group">
        <h2>Collections</h2>
        <?php foreach ($collections as $collection): ?>
          <a href="<?=base_url('collections');?>/<?=$collection['collection_slug'];?>" class="list-group-item"><?=$collection['collection_name'];?></a>
        <?php endforeach ?>
      </div>
    </div>
    
    <div class="col-xs-12 col-md-8 col-lg-9">
      <h2>Bobbleheads</h2>
      <?php if (empty($bobblehead_array)): ?>
        <p>There are no results to display.</p>
      <?php else: ?>
        <table class="table table-responsive table-striped">
        <thead>
          <tr>
            <th class="col-xs-4">Image</th>
            <?php if ($this->session->userdata('user_level') === 'admin'): ?>
            <th class="col-xs-3">Name</th>
            <th class="col-xs-2">Collection</th>
            <th class="col-xs-1">Stock</th>
            <th class="col-xs-1">Price</th>
            <th class="col-xs-1">Action</th>     
          <?php else: ?>
            <th class="col-xs-3">Name</th>
            <th class="col-xs-3">Collection</th>
            <th class="col-xs-1">Stock</th>
            <th class="col-xs-1">Price</th>
          <?php endif ?>
          </tr>
        </thead>
        <?php foreach ($bobblehead_array as $bobblehead): ?>
          <?php $bobblehead['collection'] = $this->collections_model->get_collection_by_id($bobblehead['collection_id']); ?>
          <tr>
            <td><img src="<?=base_url('img/product-upload/').$bobblehead['img_url'];?>" alt="<?=strip_tags($bobblehead['description'])?>" class="img-responsive"></td>
            <td><a href="<?=base_url('collections').'/'.$bobblehead['collection']['collection_slug'].'/item/'.$bobblehead['slug']?>"><?=$bobblehead['name']?></a></td>
            <td><?=$bobblehead['collection']['collection_name']?></td>
            <td><?=$bobblehead['stock']?></td>
            <td>$<?=$bobblehead['price']?></td>
            <?php if ($this->session->userdata('user_level') === 'admin'): ?>
              <td><a href="<?=base_url('collections/item/edit/').$bobblehead['bobblehead_id']?>">Edit</a></td>
            <?php endif ?>
          </tr>
        <?php endforeach ?>
        </table>

        <?=$this->pagination->create_links()?>

      <?php endif ?>
    </div>
  </div>
</main>
