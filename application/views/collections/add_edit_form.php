<?php
defined('BASEPATH') OR exit('No direct script access allowed!');
if (isset($collection)) $edit_id = $collection['collection_id'];
?>
<main>
  <section class="row">
    <div class="col-xs-12">
      <h1><?=$title?></h1>
    </div>
  </section>
  <section class="row">
    <?=validation_errors()?>
    <?=isset($edit_id) ? form_open('collections/edit/'.$edit_id, 'class="col-xs-12"') : form_open('collections/create', 'class="col-xs-12"')?>
      <div class="form-group">
        <label for="form_collection_name" class="control-label">Collection Name</label>
        <input type="text" name="name" id="form_collection_name" class="form-control" value="<?=set_value('name', isset($collection['collection_name']) ? $collection['collection_name'] : '')?>">
      </div>
      <div class="form-group">
        <label for="form_collection_description" class="control-label">Collection Description</label>
        <textarea name="description" id="form_collection_description"><?=isset($collection['collection_description']) ? $collection['collection_description'] : ''?></textarea>
      </div>
      <div class="form-group">
        <button type="submit" name="submit" class="btn btn-primary"><?=isset($collection) ? 'Edit Collection' : 'Create Collection'?></button>
      </div>
    </form>    
  </section>
</main>

<script src="<?=base_url('js/ckeditor/ckeditor.js')?>"></script>
<script>
  $(function() {
    CKEDITOR.replace('description');
  });
</script>
