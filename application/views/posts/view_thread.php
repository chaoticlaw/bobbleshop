<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<ol class="breadcrumb">
  <li><a href="<?=base_url('')?>">Home</a></li>
  <li><a href="<?=base_url('forum')?>">Forum</a></li>
  <li><a href="<?=base_url('forum/f/').$forum->slug?>"><?=$forum->name?></a></li>
  <li class="active"><?=$thread[0]['title']?></li>
</ol>
<main>
  <div class="row">
    <section class="col-xs-12">
      <h1><?=$title?></h1>
    </section>
  </div>
  <?php if ($this->session->userdata('logged_in')): ?>   
  <div class="row">
    <div class="col-xs-12 col-md-4 col-md-offset-8 text-right">
      <a href="<?=base_url('forum/t/'.$thread[0]['id'].'/reply')?>" class="btn btn-default">Reply</a>
    </div>
  </div>
  <?php endif ?>
  <section>
  <?php $i = 0 ?>
  <?php foreach ($thread as $post): ?>
    <div class="row">
      <div class="col-xs-12">
        <article class="panel panel-<?= ($i === 0) ? 'primary' : 'default'; ?>">
          <header class="row panel-body">
            <div class="col-xs-8">
              <h2><?= $post["name"]?></h2>
            </div>
            <div class="col-xs-4">
              <p class="text-right"><?= date('j M g:i a', strtotime($post["date_created"]))?></p>
            </div>
          </header>
          <div class="panel-body"><?= $post['body'] ?></div>
        </article>
      </div>
    </div>
    <?php $i++ ?>
  <?php endforeach ?>
  </section>
</main>
