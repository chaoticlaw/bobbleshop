<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<main>
  <section class="row">
    <?=validation_errors()?>
    <?= form_open('', 'class="col-xs-12 col-md-6"') ?>
    <h1><?=$title?></h1>
    <div class="form-group">
      <label for="title" class="control-label">Thread Name</label>
      <input type="text" id="title" name="title" value="<?=set_value('title')?>" class="form-control">
    </div>
    <div class="form-group">
      <label for="post_body">Body</label>
      <textarea name="post_body" id="post_body">
        <?=set_value('post_body')?>
      </textarea>
    </div>
    <div class="form-group">
      <button type="submit" name="create" class="btn btn-default">Create Topic</button>
    </div>
  </section>
</main>

<script src="<?=base_url('js/ckeditor/ckeditor.js')?>"></script>
<script>
  $(function() {
    CKEDITOR.replace('post_body');
  });
</script>
