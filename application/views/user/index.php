<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<main>
  <div class="row">
    <div class="col-xs-12">
      <h1>User</h1>
      <p>Hello, <?=$user->name;?>!</p>
    </div>
  </div>
  <section class="row">
    <div class="col-xs-12 col-md-6">
      <h1>Actions</h1>
      <ul class="list-group">
        <li class="list-group-item"><a href="<?=base_url('user/edit')?>"><i class="fa fa-pencil" aria-hidden="true"></i> Edit your profile</a></li>
        <li class="list-group-item"><a href="<?=base_url('custom/list')?>"><i class="fa fa-pencil"></i> Review enquiries</a></li>
        <li class="list-group-item"><a href="<?=base_url('user/logout')?>"><i class="fa fa-sign-out" aria-hidden="true"></i> Log out</a></li>
      </ul>    
    </div>
    <?php if($this->session->userdata('user_level') === 'admin'):?>
    <div class="col-xs-12 col-md-6">
      <h1>Staff Actions</h1>
      <ul class="list-group">
        <li class="list-group-item"><a href="<?=base_url('news/create')?>">Create a new News Article</a></li>
        <li class="list-group-item"><a href="<?=base_url('collections/create')?>">Create Collection</a></li>
        <li class="list-group-item"><a href="<?=base_url('bobblehead/create')?>">Create a Bobblehead</a></li>
        <li class="list-group-item"><a href="<?=base_url('user/browse/')?>">Manage users</a></li>
        <!--<li class="list-group-item">Edit collections</li>
        <li class="list-group-item">Edit bobbleheads</li>-->
        <li class="list-group-item"><a href="<?=base_url('forum/manage/')?>">Manage Forum</a></li>
      </ul>
    </div>
    <?php endif ?>
  </section>
</main>
