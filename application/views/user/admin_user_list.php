<?php
defined('BASEPATH') OR exit('No direct script access allowed!');
?>

<main>
  <section class="row">
    <div class="col-xs-12">
      <h1><?=$title?></h1>
    </div>
  </section>
  <section class="row">
    <div class="col-xs-12">
      <table class="table table-striped">
        <thead>
          <tr>
            <th class="col-xs-1">ID</th>
            <th class="col-xs-1"><div class="hidden">User Privileges</div></th>
            <th class="col-xs-5">Full Name</th>
            <th class="col-xs-4">E-mail</th>
            <th class="col-xs-1">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($users as $user): ?>
            <tr>
              <th scope="row"><?=$user['user_id']?></th>
              <td><?php if ($user['user_level'] === 'admin'): ?><i class="fa fa-shield" aria-label="Admin"></i><?php endif ?></td>
              <td><?=$user['name']?></td>
              <td><?=$user['email']?></td>
              <td><a href="<?=base_url('user/edit/').$user['user_id']?>">Edit</a></td>
            </tr>
          <?php endforeach ?>
        </tbody>
      </table>
    </div>
    <div class="col-xs-12">
      <?=$this->pagination->create_links()?>
    </div>
  </section>
</main>
