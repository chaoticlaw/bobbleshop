<?php
defined('BASEPATH') OR exit('No direct script access allowed!');

?>
<main>
  <section class="row">
    <div class="col-xs-12">
      <h1><?=$title?></h1>
    </div>
  </section>
  <section class="row">
    <?= validation_errors() ?>
    <?= form_open('user/edit/'.$user->user_id, 'class="row-xs-12 col-md-6"') ?>
      <div class="form-group">
        <label for="email" class="control-label">E-mail</label>
        <div class="input-group">
          <div class="input-group-addon"><i class="fa fa-envelope" aria-hidden="true"></i></div>
          <input type="text" id="email" name="email" class="form-control" value="<?=set_value('email', isset($user->email) ? $user->email : '') ?>">
        </div>
      </div>
      <div class="form-group">
        <label for="name" class="control-label">Full name</label>
        <input type="text" id="name" name="name" class="form-control" value="<?=set_value('name', isset($user->name) ? $user->name : '')?>">
      </div>
      <div class="form-group">
        <label for="password" class="control-label">Password, optional</label>
        <div class="input-group">
          <div class="input-group-addon"><i class="fa fa-asterisk" aria-hidden="true"></i></div>
          <input type="password" id="password" name="password" class="form-control">
        </div>
      </div>
      <div class="form-group">
        <button type="submit" name="update" value="update" class="btn btn-primary">Update details</button>
        <button type="submit" name="destroy" value="destroy" class="btn btn-danger">Delete account</button>
      </div>
    </form>
  </section>
  
</main>
