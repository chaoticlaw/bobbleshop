<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<main>
  <section class="row">
  <?php echo validation_errors(); ?>
  <?php echo form_open('user/register', 'class="col-xs-12 col-md-6"') ?>
    <h1><?=$title?></h1>
    <div class="form-group">
      <label for="email" class="control-label">E-mail address</label>
      <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-envelope" aria-hidden="true"></i></div>
        <input type="text" id="email" name="email" value="<?php echo set_value('email'); ?>" class="form-control">
      </div>
    </div>
    <div class="form-group">
      <label for="name" class="control-label">Full name</label>
      <input type="text" id="name" name="name" value="<?php echo set_value('name'); ?>" class="form-control">
    </div>
    <div class="form-group">
      <label for="password" class="control-label">Password</label>
      <input type="password" id="password" name="password" class="form-control">
    </div>
    <div class="form-group">    
      <label for="password_confirm" class="control-label">Confirm Password</label>
      <input type="password" id="password_confirm" name="password_confirm" class="form-control">
    </div>
    <div class="form-group">
      <button type="submit" name="register" class="btn btn-default">Register</button>
    </div>
  </form>
  <div class="col-xs-12 col-md-6">
    <h1>What does registering do?</h1>
    <ul>
      <li>Automatic filling in of contact information on our site</li>
      <li>You can check out your existing inquiries</li>
      <li>Update contact information</li>
      <li>And much more!</li>
    </ul>
  </div>
  </section>
</main>
