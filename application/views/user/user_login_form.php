<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<main>
  <section class="row">
  <?php echo validation_errors(); ?>
  <?php echo form_open('user/login', 'class="col-xs-12 col-md-6"') ?>
    <h1><?=$title?></h1>
    <div class="form-group">
      <label for="email" class="control-label">E-mail address</label>
      <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-envelope" aria-hidden="true"></i></div>
        <input type="text" id="email" name="email" value="<?=set_value('email')?>" class="form-control">
      </div>
    </div>
    <div class="form-group">
      <label for="password" class="control-label">Password</label>
      <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-asterisk" aria-hidden="true"></i></div>
        <input type="password" id="password" name="password" class="form-control">
      </div>
    </div>
    <div class="form-group">
      <button type="submit" name="login" class="btn btn-default">Log in</button>
    </div>
  </form>
  </section>
</main>
