<?php
defined('BASEPATH') OR exit('No direct script access allowed!');
?>
<main>
  <?php if ($this->session->userdata('user_level') === 'admin'): ?>
  <div class="row">
    <div class="col-xs-12">
      <h1>Admin actions</h1>
      <ul class="list-group">
        <li class="list-group-item"><a href="<?=base_url('collections/item/edit/').$bobblehead['bobblehead_id']?>">Edit Bobblehead</a></li>
      </ul>
    </div>
  
  </div>
  <?php endif ?>
  <section class="row">
    <div class="col-xs-12 col-md-6">
      <h1><?=$bobblehead['name'];?></h1>
      <img src="<?=base_url('img/product-upload/').$bobblehead['img_url']?>" alt="<?=$bobblehead['name']?>" class="img-responsive img-rounded">
    </div>
    <div class="col-xs-12 col-md-6">
      <h1>Shop</h1>
      <h2>$<?=$bobblehead['price']?></h2>
      <p>Stock available: <?=$bobblehead['stock']?></p>
      <?=validation_errors()?>
      <?=form_open('cart/add/'.$bobblehead['bobblehead_id'])?>
        <input type="number" id="quantity" name="quantity" value="<?=set_value('quantity')?>" max="<?=$bobblehead['stock']?>" required>
        <button type="submit">Add to Cart</button>
      </form>
    </div>
  </section>
  <section class="row">
    <div class="col-xs-12">
      <h1>Description</h1>
      <?=$bobblehead['description']?>
    </div>
  </section>
</main>

