<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($bobblehead)) $edit_id = $bobblehead['bobblehead_id'];
?>
<main>
  <section class="row">
    <div class="col-xs-12">
      <h1><?=$title?></h1>
    </div>
  </section>
  <section class="row">  
    <?=validation_errors()?>
    <?=isset($edit_id) ? form_open_multipart('bobblehead/edit/'.$edit_id, 'class="col-xs-12 col-md-6"') : form_open_multipart('bobblehead/create', 'class="row-xs-12 col-md-6"')?>
    <div class="form-group">
      <label for="form_bobblehead_name" class="control-label">Bobblehead Name</label>
      <input type="text" name="name" id="form_bobblehead_name" class="form-control" value="<?=set_value('name', isset($bobblehead['name']) ? $bobblehead['name'] : '')?>" required>
    </div>
    <div class="form-group">
      <label for="form_bobblehead_saledate" class="control-label">Date of sale</label>
      <input type="datetime-local" name="sale_date" id="form_bobblehead_saledate" class="form-control" value="<?=set_value('sale_date', isset($bobblehead['sale_date']) ? date('Y-m-d', strtotime($bobblehead['sale_date'])).'T'.date('H:i:s', strtotime($bobblehead['sale_date'])) : '')?>" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}" required>
    </div>
    <div class="form-group">
      <label for="price">Price</label>
      <div class="input-group">
        <span class="input-group-addon">$</span>
        <input type="number" step=".01" id="price" name="price" class="form-control" value="<?=set_value('price', isset($bobblehead['price']) ? $bobblehead['price'] : '')?>" required>
      </div>
    </div>
    <div class="form-group">
      <label for="stock">Product Stock</label>
      <input type="number" id="stock" name="stock" class="form-control" value="<?=set_value('stock', isset($bobblehead['stock']) ? $bobblehead['stock'] : '')?>" required>
    </div>
    <div class="form-group">
      <label for="form_bobblehead_description" class="control-label">Description</label>
      <textarea name="description" id="form_bobblehead_description"><?=set_value('description', isset($bobblehead['description']) ? $bobblehead['description'] : '' )?></textarea>
    </div>
    <div class="form-group">
      <label for="form_bobblehead_collection" class="control-label">Collection</label>
      <select name="collection_id" id="form_bobblehead_collection" class="form-control" required>
        <?php foreach ($all_collections as $collection): ?>
          <option value="<?=$collection['collection_id']?>"><?=$collection['collection_name']?></option>
        <?php endforeach ?>
      </select>
    </div>
    <div class="form-group">
      <label for="form_bobblehead_img" class="control-label">Upload Bobblehead</label>
      <input type="file" name="image" id="form_bobblehead_img">
    </div>
    <div class="form-group">
      <button type="submit" name="submit_form" class="btn btn-primary"><?=isset($bobblehead) ? 'Edit Bobblehead' : 'Create Bobblehead' ?></button>
      <?php if (isset($bobblehead)):?>
        <button type="submit" name="destroy" value="destroy" class="btn btn-danger">Delete Product</button>
      <?php endif ?>
    </div>
  </form>
  <div class="col-xs-12 col-md-6">
    <?php if (!empty($bobblehead['img_url'])): ?>
    <h3>Existing image</h3>
    <img src="<?=base_url('img/product-upload/').$bobblehead['img_url']?>" alt="" class="img-responsive">
    <?php endif ?>
  </div>
  </section>
</main>


<script src="<?=base_url('js/ckeditor/ckeditor.js')?>"></script>
<script>
  $(function() {
    CKEDITOR.replace('description');
  });
</script>
