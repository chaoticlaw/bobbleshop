<?php
defined('BASEPATH') OR exit('No direct script access allowed!');
?>
<main>
  <section class="row">
    <div class="col-xs-12">
      <h1><?=$title?></h1>
    </div>
  </section>
  <section class="row">
    <div class="col-xs-12">
      <h2>Name</h2>
      <p><?=$inquiry['user_fullname']?></p>
    </div>
    <div class="col-xs-12">
      <h2>E-mail</h2>
      <p><?=$inquiry['user_email']?></p>
    </div>
    <div class="col-xs-12">
      <h2>Description</h2>
      <?=$inquiry['custom_description']?>
    </div>
    <div class="col-xs-12">
      <h2>Reference Image</h2>
      <img src="<?=base_url('img/custom-bobbleheads/').$inquiry['img_url']?>" alt="Uploaded image" class="img-responsive img-rounded">
    </div>
  </section>
</main>
