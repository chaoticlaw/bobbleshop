<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<main>
  <section class="row">
    <div class="col-xs-12">
      <h1><?=$title?></h1>
      <p>We're glad that you want to have us design a custom bobblehead for you! Fill out the form below, and we'll get back to you as soon as possible!</p>
    </div>
  </section>
  <section class="row">
    <?=validation_errors()?>
    <?=form_open_multipart('custom/create', 'class="col-xs-12"')?>
      <div class="form-group">
        <label for="form_fullname" class="control-label">Full Name</label>
        <input type="text" name="user_fullname" id="form_fullname" class="form-control" value="<?=set_value('user_fullname', isset($user) ? $user->name : '')?>">
      </div>
      <div class="form-group">
        <label for="form_email" class="control-label">E-mail</label>
        <div class="input-group">
          <div class="input-group-addon"><i class="fa fa-envelope" aria-hidden="true"></i></div>
          <input type="text" name="user_email" id="form_email" class="form-control" value="<?=set_value('user_email', isset($user) ? $user->email : '')?>">
        </div>
      </div>
      <div class="form-group">
        <label for="form_image" class="control-label">Reference image</label>
        <input type="file" name="image" id="form_image">
      </div>
      <div class="form-group">
        <label for="form_reference_bobble" class="control-label">Style of Bobblehead body</label>
        <select name="bobblehead_id" id="form_reference_bobble" class="form-control">
          <?php foreach ($all_bobbleheads as $bobblehead): ?>
          <option value="<?=$bobblehead['bobblehead_id']?>"><?=$bobblehead['name']?></option>
          <?php endforeach ?>
        </select>
      </div>
      <div class="form-group">
        <label for="form_custom_description" class="control-label">Description</label>
        <textarea name="description" id="form_custom_description" class="form-control"></textarea>
      </div>
      <div class="form-group">
        <button type="submit" name="submit" class="btn btn-default">Send Inquiry</button>
      </div>
    </form>
  </section>
</main>
<script src="<?=base_url('js/ckeditor/ckeditor.js')?>"></script>
<script>
  $(function() {
    CKEDITOR.replace('description');
  });
</script>
