<?php
defined('BASEPATH') OR exit('No direct script access allowed!');
?>
<main>
  <section class="row">
    <div class="col-xs-12">
      <h1><?=$title?></h1>
    </div>
  </section>
  <section class="row">
    <div class="col-xs-12">
      <table class="table table-striped">
        <thead>
          <tr>
            <th class="col-xs-1">Inquiry ID</th>
            <th class="col-xs-6">Name</th>
            <th class="col-xs-5">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($submissions as $inquiry): ?>
            <tr>
              <th scope="row"><?=$inquiry['inquiry_id']?></th>
              <td><?=$inquiry['user_fullname']?></td>
              <td><a href="<?=base_url('custom/details/').$inquiry['inquiry_id']?>">Review</a></td>
            </tr>
          <?php endforeach ?>
        </tbody>
      </table>
    </div>
    <div class="col-xs-12">
      <?=$this->pagination->create_links()?>
    </div>
  </section>
</main>
