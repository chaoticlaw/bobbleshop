<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<main>
  <div class="row">
    <section class="col-xs-12">
      <h1><?=$title?></h1>
    </section>
  </div>
  <section class="row">
    <?php if (empty($cart)): ?>
      <p class="col-xs-12">There are no items in your cart! <a href="<?=base_url('/collections')?>">Shop some Bobbleheads today</a>!</p>
    <?php else: ?>
      <table class="table table-hover">
        <thead>
          <tr>
            <th class="col-xs-3">Name</th>
            <th class="col-xs-3">Quantity</th>
            <th class="col-xs-3">Price</th>
            <th class="col-xs-3">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php $subtotal = 0.00 ?>
          <?php foreach ($cart as $item): ?>
          <?=form_open('cart/update/'.$item['cart_id'])?>
          <tr>
            <td><?=$item['name']?></td>
            <td><input type="number" name="quantity" value="<?=set_value('quantity', isset($item['product_quantity']) ? $item['product_quantity'] : '')?>" required></td>
            <td>$<?=$item['price']?></td>
            <td><button name="update" value="update" type="submit" class="btn btn-primary">Update</button>&nbsp;<button name="destroy" value="destroy" class="btn btn-danger">Remove</button></td>
            <?php $subtotal = $subtotal + ($item['price'] * $item['product_quantity'])  ?>
          </tr>
          </form>
          <?php endforeach ?>
        </tbody>
      </table>
      <div class="row">
        <div class="col-xs-12 col-md-3 col-md-offset-6">
          <p class="text-right">
            Subtotal: $<?=number_format($subtotal, 2)?>
          </p>
        </div>
      </div>
      <section class="row">
        <a href="<?=base_url('cart/checkout')?>" class="btn btn-primary">Checkout</a>
        <a href="<?=base_url('collections')?>" class="btn btn-default">Continue Shopping</a>
      </section>
    <?php endif ?>
  </section>
</main>
