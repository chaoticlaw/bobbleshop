<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<main>
  <div class="row">
    <section class="col-xs-12">
      <h1><?=$title?></h1>
      <p>Note: No payment or credit card information can be submitted.</p>
    </section>
  </div>
  <div class="row">
    <section class="col-xs-12 col-md-6">
      <h1>Items</h1>
      <table class="table table-hover">
        <thead>
          <tr>
            <th class="col-xs-4">Name</th>
            <th class="col-xs-4">Quantity</th>
            <th class="col-xs-4">Price</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($cart as $item): ?>
          <?php $subtotal = 0.00 ?>
          <tr>
            <td><?=$item['name']?></td>
            <td><?=$item['product_quantity']?></td>
            <td>$<?=$item['price']?></td>
            <?php $subtotal = $subtotal + ($item['price'] * $item['product_quantity'])  ?>
          </tr>
          <?php endforeach ?>
        </tbody>
      </table>
    </section>
    <section class="col-xs-12 col-md-6">
      <?=validation_errors()?>
      <?=form_open('cart/checkout')?>
        <div class="form-group">
          <label for="name">Name</label>
          <input type="text" id="name" name="name" class="form-control" value="<?=set_value('name', isset($user) ? $user->name : '')?>" required>
        </div>
        <div class="form-group">
          <label for="email" class="control-label">E-mail</label>
          <div class="input-group">
            <div class="input-group-addon"><i class="fa fa-envelope" aria-hidden="true"></i></div>
            <input type="text" name="email" id="email" class="form-control" value="<?=set_value('email', isset($user) ? $user->email : '')?>" required>
          </div>
        </div>
        <div class="form-group">
          <label for="name">Address</label>
          <input type="text" name="address" class="form-control" value="<?=set_value('address')?>" required>
        </div>
        <div class="form-group">
          <label for="suburb">Suburb</label>
          <input id="suburb" type="text" name="suburb" class="form-control" value="<?=set_value('suburb')?>" required>
        </div>
        <div class="form-group">
          <label for="postcode">Postcode</label>
          <input id="postcode" type="text" name="postcode" class="form-control" value="<?=set_value('postcode')?>" required>
        </div>
        <div class="form-group">
          <label for="aus_state">State</label>
          <select id="aus_state" name="aus_state" class="form-control">
            <option value="NSW">New South Wales</option>
            <option value="ACT">Australian Capital Territory</option>
            <option value="TAS">Tasmania</option>
            <option value="VIC">Victoria</option>
            <option value="SA">South Australia</option>
            <option value="WA">Western Australia</option>
            <option value="NT">Northern Terrritory</option>
            <option value="QLD">Queensland</option>
          </select>
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-primary">Checkout</button>
        </div>
      </form>
    </section>

  </div>
</main>
