<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<ol class="breadcrumb">
  <li><a href="<?=base_url('')?>">Home</a></li>
  <li><a href="<?=base_url('forum')?>">Forum</a></li>
  <li><?=$title?></li>
</ol>
<main>
  <div class="row">
    <section class="col-xs-12">
      <h1><?=$title?></h1>
      <?= !empty($description) ? '<p>' . $description . '</p>' : '' ?>
    </section>
  </div>
  <?php if ($this->session->userdata('logged_in')):?>
  <div class="row">
    <div class="col-xs-12 col-md-4 col-md-offset-8 text-right">
      <a href="<?=base_url('forum/f/'.$forum->slug.'/new_topic')?>" class="btn btn-default">New Topic</a>
      <?php if ($this->session->userdata('user_level') === 'admin'): ?>
        <a href="<?=base_url('forum/edit/'.$forum->id)?>" class="btn btn-default">Manage Forum</a>
      <?php endif ?>
    </div>
  </div>
  <?php endif ?>
  <section class="row">
    <div class="col-xs-12 col-md-6">
      <h2>Topics</h2>
    </div>
  </section>
  <section class="row">
    <div class="col-xs-12">
      <?php if (empty($topics)): ?>
        <div class="panel panel-info">
          <div class="panel-body">
            <p>There are currently no topics.</p>
          </div>
        </div>
      <?php else: ?>
        <table class="table">
          <thead>
            <tr>
              <th class="col-xs-6">Topic</th>
              <th class="col-xs-2">Author</th>
              <th class="col-xs-2">Created</th>
              <th class="col-xs-2">Replies</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($topics as $topic): ?>
              <tr>
                <td><a href="<?= base_url('forum/t/'.$topic['id'].'/'.$topic['slug']) ?>"><?= $topic['title'] ?></a></td>
                <td><?= $topic['name'] ?></td>
                <td><?= date('j M g:i a', strtotime($topic['date_created'])) ?></td>
                <td><?= $post_model->count_replies_of_thread($topic['id']) ?></td>
              </tr>
            <?php endforeach ?>
          </tbody>
        </table>
      <?php endif ?>
    </div>
  </section>
</main>
