<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<main>
  <section class="row">
    <?=validation_errors()?>
    <?php echo form_open('forum/create', 'class="col-xs-12 col-md-6"') ?>
    <h1><?=$title?></h1>
    <div class="form-group">
      <label for="name" class="control-label">Forum Name</label>
      <input type="text" id="name" name="name" value="<?=set_value('name')?>" class="form-control">
    </div>
    <div class="form-group">
      <label for="slug" class="control-label">Slug (optional)</label>
      <input type="text" id="slug" name="slug" value="<?=set_value('slug')?>" class="form-control">
    </div>
    <div class="form-group">
      <label for="colour" class="control-label">Label Colour</label>
      <input type="colour" id="colour" name="colour" value="<?=set_value('colour')?>" class="form-control">
    </div>
    <div class="form-group">
      <label for="description" class="control-label">Description</label>
      <input type="text" id="description" name="description" value="<?=set_value('description')?>" class="form-control">
    </div>
    <div class="form-group">
      <button type="submit" name="create" class="btn btn-default">Create Forum</button>
    </div>
  </section>
</main>
