<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<main>
  <section class="row">
    <?= validation_errors() ?>
    <?= form_open('forum/edit/'.$forum->id, 'class="row-xs-12 col-md-6"') ?>
    <h1><?=$title?></h1>
      <div class="form-group">
        <label for="name" class="control-label">Forum Name</label>
        <input type="text" id="name" name="name" value="<?=set_value('name', isset($forum->name) ? $forum->name : '') ?>" class="form-control">
      </div>
      <div class="form-group">
        <label for="slug" class="control-label">Slug (optional)</label>
        <input type="text" id="slug" name="slug" value="<?=set_value('slug', isset($forum->slug) ? $forum->slug : '') ?>" class="form-control">
      </div>
      <div class="form-group">
        <label for="colour" class="control-label">Label Colour</label>
        <input type="colour" id="colour" name="colour" value="<?=set_value('colour', isset($forum->colour_code) ? $forum->colour_code : '') ?>" class="form-control">
      </div>
      <div class="form-group">
        <label for="description" class="control-label">Description</label>
        <input type="text" id="description" name="description" value="<?=set_value('description', isset($forum->description) ? $forum->description : '') ?>" class="form-control">
      </div>
      <div class="form-group">
        <button type="submit" name="update" value="update" class="btn btn-primary">Update Forum</button>
        <button type="submit" name="destroy" value="destroy" class="btn btn-danger">Delete Forum</button>
      </div>
    </form>
  </section>
  
</main>
