<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<main>
  <div class="row">
    <section class="col-xs-12">
      <h1><?=$title?></h1>
    </section>
  </div>
  <?php if ($this->session->userdata('logged_in') && $this->session->userdata('user_level') === 'admin'):?>
  <div class="row">
    <div class="col-xs-12 col-md-4 col-md-offset-8 text-right">
      <a href="<?= base_url('forum/create') ?>" class="btn btn-default">New Forum</a>
    </div>
  </div>
  <?php endif ?>
  <section class="row">
    <div class="col-xs-12 col-md-6">
      <h2>Categories</h2>
      <div class="row">
        <div class="col-xs-12">
          <table class="table">
            <thead>
              <tr>
                <th class="col-xs-1">ID</th>
                <th class="col-xs-8">Title</th>
                <th class="col-xs-3">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($forums as $category): ?>
                <tr style="border-left: 4px solid <?= $category['colour_code'] ?>">
                  <td><?=$category['id']?></td>
                  <td><?=$category['name']?></td>
                  <td><a href="<?=base_url('forum/edit/'.$category['id'])?>">Edit</a></td>
                </tr>
              <?php endforeach ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </section>
</main>
