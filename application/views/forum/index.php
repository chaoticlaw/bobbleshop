<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<main>
  <div class="row">
    <section class="col-xs-12">
      <h1><?=$title?></h1>
    </section>
  </div>
  <?php if ($this->session->userdata('logged_in') && $this->session->userdata('user_level') === 'admin'):?>
  <div class="row">
    <div class="col-xs-12 col-md-4 col-md-offset-8 text-right">
      <a href="<?= base_url('forum/create') ?>" class="btn btn-default">New Forum</a>
      <?php if ($this->session->userdata('user_level') === 'admin'): ?>
        <a href="<?=base_url('forum/manage/')?>" class="btn btn-default">Manage Forums</a>
      <?php endif ?>
    </div>
  </div>
  <?php endif ?>
  <section class="row">
    <div class="col-xs-12 col-md-6">
      <h2>Categories</h2>
      <?php foreach ($forums as $category): ?>  
      <div class="row">
        <div class="col-xs-12" style="border-left: 4px solid <?= $category['colour_code'] ?>" class="forum_category">
          <a href="<?= base_url('forum/f/'.$category['slug'])?>" class="forum_category__link"><?= $category['name'] ?></a>
          <?php if (!empty($category['description'])): ?>
          <p><?= $category['description'] ?></p>
          <?php endif ?>
        </div>
      </div>
      <?php endforeach ?>
    </div>
    <div class="col-xs-12 col-md-6">
      <h2>Recent Topics</h2>
      <div class="row">
        <div class="col-xs-12 list-group">
          <?php foreach ($recent_topics as $post): ?>
          <a class="list-group-item" href="<?= base_url('forum/t/'.$post['id'].'/'.$post['slug']) ?>"><?= $post['title'] ?></a>
          <?php endforeach ?>
        </div>
      </div>
    </div>
  </section>
</main>
