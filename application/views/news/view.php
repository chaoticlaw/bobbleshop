<?php
defined('BASEPATH') OR exit('No direct script access allowed!');
?>
<main>
  <div class="row">
    <div class="col-xs-12"><h1><?=$news_item['news_title'];?></h1>
      <?=$news_item['news_content'];?></div>
  </div>
</main>
