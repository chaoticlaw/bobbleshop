<?php
defined('BASEPATH') OR exit('No direct script access allowed!');

?>
<h1><?=$title?></h2>
<?php
foreach($news as $news_item):
$news_url = site_url('news/'.$news_item['post_year'].'/'.$news_item['post_month'].'/'.$news_item['slug']);
?>
<article>
  <h2><a href="<?php echo $news_url; ?>"><?php echo $news_item['news_title']; ?></a></h2>
  <section>
    <h3><?php echo date("jS F, Y", strtotime($news_item['news_date'])); ?></h3>
    <p><a href="<?php echo $news_url ?>">Read article</a></p>
  </section>
</article>
<?php endforeach;?>
