<?php
defined('BASEPATH') OR exit('No direct script access allowed!');
  /*
   * form_open() is powered by the form helper which we can plug extra things into
   * validation_errors() is pretty straight forward.
   */
  if (isset($news_item)) {
    $edit_id = $news_item['news_id'];
  }
?>
<main>
  <section class="row">
    <div class="col-xs-12">
      <h1><?=$title?></h1>
    </div>
  </section>
  <section class="row">
    <?= validation_errors() ?>
    <?= isset($edit_id) ? form_open('news/edit/'.$edit_id, 'class="col-xs-12"') : form_open('news/create', 'class="col-xs-12"') ?>
    <?php if (isset($edit_id)): ?>
      <p>Editing article <?=$edit_id?></p>    
    <?php endif ?>
    <div class="form-group">
      <label for="form_newstitle" class="control-label">Title</label>
      <input type="text" name="title" id="form_newstitle" class="form-control" value="<?=set_value('title', isset($news_item['news_title']) ? $news_item['news_title'] : '')?>" required>
    </div>
  
    <div class="form-group">
      <label for="form_newsdate" class="control-label">Publish date</label>
      <input type="datetime-local" name="date" id="form_newsdate" class="form-control" value="<?=set_value('date', isset($news_item['news_date']) ? date("Y-m-d\TH:i:s", strtotime($news_item['news_date'])) : '')?>" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}" required>
    </div>
  
    <div class="form-group">
      <label for="form_newscontent" class="control-label">Text</label>
      <textarea name="content" id="form_newscontent"><?=set_value('content', isset($news_item['news_content']) ? $news_item['news_content'] : '') ?></textarea>
    </div>
    <div class="form-group">
      <button type="submit" name="submit" class="btn btn-primary"><?= isset($news_item) ? 'Edit news item' : 'Create news item' ?></button>
      <?php if (isset($news_item)) :?>
      <button type="submit" name="delete" class="btn btn-danger">Delete article</button>
      <?php endif; ?>
    </div>
  </form>  
  </section>
</main>
<script src="<?=base_url('js/ckeditor/ckeditor.js')?>"></script>
<script>
  $(function() {
    CKEDITOR.replace('content');
  });
</script>
