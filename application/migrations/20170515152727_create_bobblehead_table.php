<?php
defined('BASEPATH') OR exit('No direct script access allowed!');

class Migration_Create_Bobblehead_Table extends CI_Migration {

  public function up() {
    $this->dbforge->add_field(array(
      'bobblehead_id' => array(
        'type'            => 'INT',
        'constraint'      => 5,
        'unsigned'        => TRUE,
        'auto_increment'  => TRUE
      ),
      'collection_id' => array(
        'type'            => 'INT',
        'constraint'      => 5,
        'unsigned'        => TRUE,
        'null'            => TRUE
      ),
      'name'          => array(
        'type'            => 'VARCHAR',
        'constraint'      => 255,
        'null'            => FALSE
      ),
      'description'   => array(
        'type'            => 'TEXT',
        'null'            => FALSE
      ),
      'sale_date'     => array(
        'type'            => 'DATETIME',
        'null'            => TRUE
      ),
      'img_url'       => array(
        'type'            => 'VARCHAR',
        'constraint'      => 255,
        'null'            => TRUE
      ),
      'slug'          => array(
        'type'            => 'VARCHAR',
        'constraint'      => 255,
        'null'            => FALSE
      )
    ));
    $this->dbforge->add_key('bobblehead_id', TRUE);
    $this->dbforge->create_table('bobblehead');
  }

  public function down() {
    $this->dbforge->drop_table('bobblehead');
  }
}
