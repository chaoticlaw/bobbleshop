<?php

// For security purposes, never let this be viewed in browser directly
defined('BASEPATH') OR exit('No direct script access allowed!');

class Migration_Add_Slugs_to_News extends CI_Migration {
  // function up() indicates behaviour when migrating
  // set id to unsigned since it will never be negative
  public function up() {
    $fields = array(
      'slug'  => array(
        'type'        => 'varchar',
        'constraint'  => 144
      )
    );
    $this->dbforge->add_column('news', $fields);
    $this->dbforge->add_key('slug');
  }

  // function down() indicates what to do to reverse changes made by up()
  public function down() {
    $this->dbforge->drop_column('news', 'slug');
  }
}
?>