<?php
// For security purposes, never let this be viewed in browser directly
defined('BASEPATH') OR exit('No direct script access allowed!');

class Migration_Create_News_Table extends CI_Migration {
  // function up() indicates behaviour when migrating
  // set id to unsigned since it will never be negative
  public function up() {
    $this->dbforge->add_field(array(
      'news_id' => array(
        'type'            => 'INT',
        'constraint'      => 5,
        'unsigned'        => TRUE,
        'auto_increment'  => TRUE
      ),
      'news_title' => array(
        'type'            => 'VARCHAR',
        'constraint'      => 144
      ),
      'news_date' => array(
        'type'            => 'DATETIME',
        'null'            => FALSE
      ),
      'news_content' => array(
        'type'            => 'TEXT',
        'null'            => TRUE
      )
    ));
    $this->dbforge->add_key('news_id', TRUE);
    $this->dbforge->create_table('news');
  }

  // function down() indicates what to do to reverse changes made by up()
  public function down() {
    $this->dbforge->drop_table('news');
  }
}
?>