<?php

defined('BASEPATH') OR exit('No direct script access allowed!');

class Migration_Create_News_View extends CI_Migration {

	// We can't use dbforge to create a view, so we have to manually query the database engine
	// This syntax works with MySQL, but isn't likely to work on other systems without a bit of tweaking.
	public function up() {
		$this->db->query('
		CREATE VIEW view_nice_news
		    AS
		SELECT news_title, news_content, news_date, slug
		     , DATE_FORMAT(news_date, \'%Y\') AS post_year
		     , DATE_FORMAT(news_date, \'%m\') AS post_month
		FROM news
		');
	}

	public function down() {
		$this->db->query('DROP VIEW view_nice_news');
	}
}