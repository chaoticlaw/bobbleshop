<?php
defined('BASEPATH') OR exit('No direct script access allowed!');

class Migration_Add_Shopping_To_Bobblehead extends CI_Migration {
  public function up() {
    $fields = array(
      'price' => array(
        'type'        => 'DECIMAL',
        'constraint'  => '6,2',
        'null'        => FALSE,
      ),
      'stock' => array(
        'type'        => 'INT',
        'null'        => FALSE,
        'default'     => 0
      )
    );
    $this->dbforge->add_column('bobblehead', $fields);
  }

  public function down() {
    $this->dbforge->drop_column('bobblehead', 'price');
    $this->dbforge->drop_column('bobblehead', 'stock');
  }
}
