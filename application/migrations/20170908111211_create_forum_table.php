<?php
defined('BASEPATH') OR exit('No direct script access allowed!');

class Migration_Create_Forum_Table extends CI_Migration {
  public function up() {
    $this->dbforge->add_field(array(
      'id'            => array(
        'type'            => 'INT',
        'unsigned'        => TRUE,
        'auto_increment'  => TRUE
      ),
      'name'   => array(
        'type'            => 'VARCHAR',
        'constraint'      => 191,
        'null'            => FALSE,
        'unique'          => TRUE
      ),
      'description'   => array(
        'type'            => 'VARCHAR',
        'constraint'      => 191,
        'null'            => TRUE
      ),
      'slug'          => array(
        'type'            => 'VARCHAR',
        'constraint'      => 191,
        'null'            => FALSE
      ),
      'colour_code'   => array(
        'type'            => 'VARCHAR',
        'constraint'      => 64,
        'default'         => '#fff'
      ),
      'date_created'  => array(
        'type'        => 'DATETIME'
      ),
      'date_modified' => array(
        'type'        => 'DATETIME'
      )
    ));
    $this->dbforge->add_key('id', TRUE);
    $this->dbforge->create_table('forum');
  }

  public function down() {
    $this->dbforge->drop_table('forum');
  }
}
