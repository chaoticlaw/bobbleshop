<?php
defined('BASEPATH') OR exit('No direct script access allowed!');

class Migration_Add_Day_to_News_View extends CI_Migration {

  public function up() {
    // Remove the old view
    $this->db->query('DROP VIEW view_nice_news');

    // Recreate it
    $this->db->query('
    CREATE VIEW view_nice_news
        AS
    SELECT news_title, news_content, news_date, slug
         , DATE_FORMAT(news_date, \'%Y\') AS post_year
         , DATE_FORMAT(news_date, \'%m\') AS post_month
         , DATE_FORMAT(news_date, \'%d\') AS post_day
    FROM news
    ');
  }

  public function down() {
    $this->db->query('DROP VIEW view_nice_news');
  }
}