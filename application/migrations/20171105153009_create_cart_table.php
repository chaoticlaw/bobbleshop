<?php
defined('BASEPATH') OR exit('No direct script access allowed!');

class Migration_Create_Cart_Table extends CI_Migration {
  public function up() {
    $this->dbforge->add_field(array(
      'cart_id' => array(
        'type'            => 'INT',
        'constraint'      => 5,
        'unsigned'        => TRUE,
        'auto_increment'  => TRUE
      ),
      'session_id' => array(
        'type'            => 'VARCHAR',
        'constraint'      => 191
      ),
      'product_id' => array(
        'type'            => 'INT',
        'null'            => FALSE
      ),
      'product_quantity' => array(
        'type'            => 'INT',
        'null'            => TRUE
      ),
      'date_added' => array(
        'type'            => 'TIMESTAMP'
      )
    ));
    $this->dbforge->add_key('cart_id', TRUE);
    $this->dbforge->create_table('cart');
  }

  public function down() {
    $this->dbforge->drop_table('cart');
  }
}
