<?php
defined('BASEPATH') OR exit('No direct script access allowed!');

class Migration_Create_Collections_Table extends CI_Migration {

  public function up() {
    $this->dbforge->add_field(array(
      'collection_id' => array(
        'type'            => 'INT',
        'constraint'      => '5',
        'unsigned'        => TRUE,
        'auto_increment'  => TRUE  
      ),
      'collection_name' => array(
        'type'            => 'VARCHAR',
        'constraint'      => 255,
        'null'            => FALSE
      ),
      'collection_description' => array(
        'type'            => 'TEXT',
        'null'            => FALSE
      ),
      'collection_slug' => array(
        'type'            => 'VARCHAR',
        'constraint'      => 255,
        'null'            => FALSE
      )
    ));

    $this->dbforge->add_key('collection_id');
    $this->dbforge->create_table('collections');
  }

  public function down() {
    $this->dbforge->drop_table('collections');
  }
}