<?php
defined('BASEPATH') OR exit('No direct script access allowed!');

class Migration_Create_User_Table extends CI_Migration {

  public function up() {
    $this->dbforge->add_field(array(
      'user_id' => array(
        'type'            => 'INT',
        'constraint'      => 5,
        'unsigned'        => TRUE,
        'auto_increment'  => TRUE  
      ),
      'name' => array(
        'type'            => 'VARCHAR',
        'constraint'      => 255,
        'null'            => FALSE
      ),
      'email' => array(
        'type'            => 'VARCHAR',
        'constraint'      => 254
      ),
      'auth_hashsalt' => array(
        'type'            => 'VARCHAR',
        'constraint'      => 255,
        'null'            => FALSE
      ),
      'user_level' => array(
        'type'            => 'VARCHAR',
        'constraint'      => 255,
        'null'            => FALSE
      )
    ));
    $this->dbforge->add_key('user_id', TRUE);
    $this->dbforge->create_table('user');
  }

  public function down() {
    $this->dbforge->drop_table('user');
  }
}