<?php
defined('BASEPATH') OR exit('No direct script access allowed!');

class Migration_Add_Date_to_Inquiries extends CI_Migration {
  public function up() {
    $fields = array(
      'inquiry_date'  => array(
        'type'        => 'TIMESTAMP'
      )
    );
    $this->dbforge->add_column('inquiries', $fields);
  }

  public function down() {
    $this->dbforge->drop_column('inquiries', 'inquiry_date');
  }
}
