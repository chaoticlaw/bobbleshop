<?php
defined('BASEPATH') OR exit('No direct script access allowed!');

class Migration_Create_Inquiries_Table extends CI_Migration {
  
  public function up() {
    $this->dbforge->add_field(array(
      'inquiry_id' => array(
        'type'            => 'INT',
        'constraint'      => '5',
        'unsigned'        => TRUE,
        'auto_increment'  => TRUE  
      ),
      'user_id' => array(
        'type'            => 'INT',
        'constraint'      => '5',
        'unsigned'        => TRUE,
        'null'            => TRUE
      ),
      'user_fullname' => array(
        'type'            => 'VARCHAR',
        'constraint'      => 255,
        'null'            => FALSE
      ),
      'user_email' => array(
        'type'            => 'VARCHAR',
        'constraint'      => 255,
        'null'            => FALSE
      ),
      'custom_description' => array(
        'type'            => 'TEXT',
        'null'            => FALSE
      ),
      'img_url' => array(
        'type'            => 'VARCHAR',
        'constraint'      => 255,
        'null'            => TRUE
      ),
      'bobblehead_id' => array(
        'type'            => 'INT',
        'constraint'      => '5',
        'unsigned'        => TRUE,
        'null'            => FALSE
      )
    ));

    $this->dbforge->add_key('inquiry_id');
    $this->dbforge->create_table('inquiries');
  }

  public function down() {
    $this->dbforge->drop_table('inquiries');
  }
}
