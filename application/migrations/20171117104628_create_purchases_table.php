<?php
defined('BASEPATH') OR exit('No direct script access allowed!');

class Migration_Create_Purchases_Table extends CI_Migration {
  public function up() {
    $this->dbforge->add_field(array(
      'purchase_id' => array(
        'type'            => 'INT',
        'unsigned'        => TRUE,
        'auto_increment'  => TRUE
      ),
      'email' => array(
        'type'            => 'VARCHAR',
        'constraint'      => 191
      ),
      'user_id' => array(
        'type'            => 'INT',
        'null'            => TRUE
      ),
      'name' => array(
        'type'            => 'VARCHAR',
        'constraint'      => 191,
        'null'            => FALSE
      ),
      'address' => array(
        'type'            => 'VARCHAR',
        'constraint'      => 191,
        'null'            => FALSE
      ),
      'suburb' => array(
        'type'            => 'VARCHAR',
        'constraint'      => 191,
        'null'            => FALSE
      ),
      'postcode' => array(
        'type'            => 'VARCHAR',
        'constraint'      => 4,
        'null'            => FALSE
      ),
      'aus_state' => array(
        'type'            => 'VARCHAR',
        'constraint'      => 10,
        'null'            => FALSE
      ),
      'cost_total' => array(
        'type'            => 'DECIMAL',
        'constraint'      => '10,2',
        'default'         => '0.00',
        'null'            => FALSE
      ),
      'date_added' => array(
        'type'            => 'TIMESTAMP'
      )
    ));
    $this->dbforge->add_key('purchase_id', TRUE);
    $this->dbforge->create_table('purchases');
  }

  public function down() {
    $this->dbforge->drop_table('purchases');
  }
}
