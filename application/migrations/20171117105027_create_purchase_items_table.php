<?php
defined('BASEPATH') OR exit('No direct script access allowed!');

class Migration_Create_Purchase_Items_Table extends CI_Migration {
  public function up() {
    $this->dbforge->add_field(array(
      'id' => array(
        'type'            => 'INT',
        'unsigned'        => TRUE,
        'auto_increment'  => TRUE
      ),
      'purchase_id' => array(
        'type'            => 'VARCHAR',
        'constraint'      => 191
      ),
      'item_id' => array(
        'type'            => 'INT',
        'null'            => FALSE
      ),
      'quantity' => array(
        'type'            => 'INT',
        'null'            => FALSE
      ),
      'unit_price' => array(
        'type'            => 'DECIMAL',
        'constraint'      => '6,2',
        'null'            => FALSE
      ),
      'date_added' => array(
        'type'            => 'TIMESTAMP'
      )
    ));
    $this->dbforge->add_key('id', TRUE);
    $this->dbforge->create_table('purchase_items');
  }

  public function down() {
    $this->dbforge->drop_table('purchase_items');
  }
}
