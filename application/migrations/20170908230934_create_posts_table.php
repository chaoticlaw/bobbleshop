<?php
defined('BASEPATH') OR exit('No direct script access allowed!');

class Migration_Create_Posts_Table extends CI_Migration {
  public function up() {
    $this->dbforge->add_field(array(
      'id'            => array(
        'type'            => 'INT',
        'unsigned'        => TRUE,
        'auto_increment'  => TRUE
      ),
      'forum_id'       => array(
        'type'            => 'INT',
        'null'            => FALSE,
        'unsigned'        => TRUE
      ),
      'user_id'       => array(
        'type'            => 'INT',
        'null'            => FALSE,
        'unsigned'        => TRUE
      ),
      'reply_id'      => array(
        'type'            => 'INT',
        'unsigned'        => TRUE,
        'null'            => TRUE
      ),
      'title'         => array(
        'type'            => 'VARCHAR',
        'constraint'      => 191,
        'null'            => FALSE,
      ),
      'slug'          => array(
        'type'            => 'VARCHAR',
        'constraint'      => 191,
        'null'            => FALSE
      ),
      'body'          => array(
        'type'            => 'TEXT',
        'null'            => FALSE
      ),
      'date_created'  => array(
        'type'        => 'DATETIME'
      ),
      'date_modified' => array(
        'type'        => 'DATETIME'
      )
    ));
    $this->dbforge->add_key('id', TRUE);
    $this->dbforge->create_table('forum_posts');
  }

  public function down() {
    $this->dbforge->drop_table('forum_posts');
  }
}
