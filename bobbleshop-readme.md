# HTML + Scripting Project (Semester 1 2017)
## By Quang Minh Nguyen

# Getting up and running

Copy this directory into your www directory. Where it is located is important since some configuration is necessary. For example, if your directory is `www/bobblehead`, we need to note the URL for the site is `http://localhost/bobblehead` (note no trailing slash).

Then open up `application/config/config.php` and set `$config['base_url']` to the URL of the site:

    $config['base_url'] = 'http://localhost/bobblehead';

## Database

You'll need to additionally set up a new database in MySQL, with a collation of `utf8_general_ci`. For example, `mnguyen_bobbleshop`.

Open up `application/config/database.php` and set the `port`, `username`, `password`, and `database` keys to the configuration of your local system. For example:

    $db['default'] = array(
      ...
      'port'      =>  '3306',
      'username'  =>  'root',
      'password'  =>  '',
      'database'  =>  'mnguyen_bobbleshop'
      ...
    );

## Importing Test Data

Going back to MySQL, import `database.sql` into `mnguyen_bobbleshop`. It contains a series of sample records to get started with. Of course, if you run into this error:

    Ending quote ' was expected.

This means that you've run into a bug with an old version of phpMyAdmin^[1]. You'll need to import the SQL file manually using the MySQL console executable:

    C:\> C:\wamp\bin\mysql5.7.11\bin\mysql.exe -u root mnguyen_bobbleshop < database.sql

Substitute 'root' with a different MySQL user if necessary, and add `-p password` if a password is needed.

[1]: https://stackoverflow.com/questions/33329405/existing-mysql-database-is-not-importing-to-localhost

## Logging in and using the site
Go to http://localhost/bobblehead.

The first user john@smith.local is set as an admin of the site and thus has a lot of extra tools compared to a regular user. To login, specify the following:

  - E-mail:   john@smith.local
  - Password: john

This e-mail/password convention is used for almost all users on the site.

# Libraries and Frameworks used

  - Codeigniter 3 (PHP Framework)
  - HTML Purifier (PHP library to sanitise HTML inputs)
    - (application/helpers/htmlpurifier_helper.php)
    - (application/third_party/HTMLPurifier.standalone.php)
  - CKEditor (RTF text editor for textareas) - (js/ckeditor)
  - Bootstrap 3 (CSS Framework) - CDN (application/views/templates/header.php)
  - jQuery (JS Framework) - CDN (application/views/templates/header.php)
  - Flickity (Carousels, slideshows) - CDN (application/views/templates/header.php)
  - EaselJS, TweenJS (Canvas banner) - CDN (application/views/templates/header.php)

# Tasks attempted

## 3. Login Feature

Login functionality is provided through the User model, views and controllers (MVC).

Relevant files:

  - application/controllers/User.php: Controller functions for navigation
  - application/models/User_model.php: Database functions
  - application/views/user: Forms and other views for users

## 4. Autofill

The enquiry form located at `http://localhost/bobblehead/custom` automatically fills in the name and e-mail fields, using session data set upon login.

Relevant files:

  - application/views/custom/inquiry_form.php

## 6. Hover custom tooltip

Tooltips are a component provided by Bootstrap, and are integrated into the Bobblehead Gallery under item 10.

Relevant files:

  - application/views/pages/home.php

## 7. Search functionality

Searching can be done in the header of the site. It first checks that the search is not empty, then uses a `LIKE` operation to retrieve results. This could be expanded to search the description in addition to the name, but this has not been implemented at this time.

Relevant files:

  - application/views/templates/header.php
  - application/models/Bobblehead_model.php

## 10. Recommended Bobbleheads Gallery

This is a gallery that allows a user to move back and forwards through recent bobbleheads.

Relevant files:

  - application/views/pages/home.php

## 11. News articles listed by headline

Selecting a headline from the index page will expand it to show the full story.

Relevant files:

  - application/views/pages/home.php

# HTML Validation

Pages that have been validated include:

  - localhost/bobblehead/
  - localhost/bobblehead/custom
  - localhost/bobblehead/about
  - localhost/bobblehead/news
  - localhost/bobblehead/contact
  - localhost/bobblehead/collections/star-wars/item/darth-vader

CSS that has been validated includes

  - localhost/css/main.css

# Other code

There is other code functionality that is out of the scope of this assignment. Namely, the following features are listed:

  - User registration, editing, deletion: Users can register and edit their own details
  - News articles: Create, edit, delete news articles
  - Bobblehead collection and bobbleheads: Create and edit bobblehead collections as well as items

Other files that may be of interest include:

  - application/config/routes.php: Contains routing rules
  - application/views/templates: Contains HTML templates for the header and footer
  - scss: Contains SCSS files that compile to the css directory
  - application/migrations: contains code used in the database migration to create the tables and entries.

# Things that aren't complete

  - The relationship between collections and bobbleheads is not established in the database, so deleting a collection will lead to erronous behaviour
  - News browsing doesn't happen with date filters (year, month, day)
  - When editing bobbleheads or news items, the datetime-local field is prepopulated by the database without the required T separator, leading to database updates of '0000-00-00 00:00:00'
  - The input of `datetime-local` doesn't have support in most browsers, and I have not enabled functionality to make it use a custom widget.
  - The sale_date of Bobbleheads isn't used yet
