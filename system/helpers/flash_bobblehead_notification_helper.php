<?php

defined('BASEPATH') OR exit('No direct script access allowed');

if (!function_exists('flash_bobble_notification')) {
  /**
   * 
   * @return void
   */
  function flash_bobble_notification($type = 'error', $message) {
    $CI =& get_instance();

    $CI->session->set_flashdata('message_type', $type);
    $CI->session->set_flashdata('message', $message);
  }
}
